<?php

namespace GuiTests;

/**
 * Description of TestLogin
 *
 * @author namax
 */
class AddUrlTest extends \PHPUnit_Framework_TestCase
{


    /**
     * @var \WebDriver
     */
    protected $webDriver;

    public function setUp()
    {
        $capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
        $this->webDriver = \RemoteWebDriver::create(SELENIUM_SERVER, $capabilities);
    }

    public function testAddUrl()
    {

        $loginPage = new \Uitests\PageObjects\LoginPage($this->webDriver);
        $entityUrl = new \Entities\Urls();

        $data['url'] = "http://google.com";
        $data['user_id'] = "2";
        $data['title'] = "goo";
        $data['time_to_show'] = "5";
        $data['shows_per_hour'] = "4";
        $data['status'] = 1;
        $data['referers'] = "http://google.com\r\nhttp://yahoo.com\r\nhttp://google.ru";

        $entityUrl->fromArray($data);
        $loginPage->login("namax25@gmail.com", "35750179")->clickToAddNewSiteLink()->addNewSite($entityUrl);
        sleep(5);
    }


    public function tearDown()
    {
        $this->webDriver->close();
    }

}
