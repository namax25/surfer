<?php

namespace GuiTests;

/**
 * Description of TestLogin
 *
 * @author namax
 */
class LoginTest extends \PHPUnit_Framework_TestCase
{


    protected $captureScreenshotOnFailure = true;
    protected $screenshotPath = '/home/namax/Projects';

    /**
     * @var \WebDriver
     */
    protected $webDriver;

    public function setUp()
    {
        $capabilities = array(\WebDriverCapabilityType::BROWSER_NAME => 'firefox');
        $this->webDriver = \RemoteWebDriver::create(SELENIUM_SERVER, $capabilities);
    }

    public function testLoginFormExists()
    {

        $this->webDriver->get(BASE_URL);


        $loginPage = new \Uitests\PageObjects\LoginPage($this->webDriver);
        $loginPage->login("namax25@gmail.com", "35750179");
        $this->assertEquals("Управление сайтами", $loginPage->getPageHeader());
    }


    public function tearDown()
    {
        $status = $this->getStatus();
        if ($status == \PHPUnit_Runner_BaseTestRunner::STATUS_ERROR || $status == \PHPUnit_Runner_BaseTestRunner::STATUS_FAILURE) {
            $file_name = str_replace("\\", "_",
                    __CLASS__) . '_' . $this->getName() . '_' . date('Y-m-d_H:i:s') . '.png';
            $this->webDriver->takeScreenshot("/home/namax/Projects/" . $file_name);
        }
        $this->webDriver->close();
    }

}
