<?php

namespace Proxy\Repository;

/**
 * Description of Url
 *
 * @author namax
 */
class File implements ProxyRepositoryInterface
{

    private $url = null;
    private $isRenewable = false;

    public function __construct($url, $isRenewable = false)
    {
        $this->url = $url;
        $this->isRenewable = $isRenewable;
    }

    /**
     *
     * @return  \Collections\Proxy\ProxySet
     */
    public function getAll()
    {
        $proxy = $this->getProxyArr($this->url);
        $list = [];

        foreach ($proxy as $item) {
            $proxyTemp = $this->parse($item);
            if ($proxyTemp && isset($proxyTemp['url']) && $proxyTemp['port']) {
                $list[] = new \Entity\Proxy($proxyTemp['url'], $proxyTemp['port'], $proxyTemp['login'],
                    $proxyTemp['password']);
            }
        }
        return $list;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getOne()
    {
        throw new \BadMethodCallException("Not implemented");
    }

    /**
     * @codeCoverageIgnore
     */
    public function update(\Entity\Proxy $proxy, $code, $msg = null)
    {

    }

    /**
     * @codeCoverageIgnore
     */
    protected function getProxyArr($url)
    {
        $arr = file($url, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        if (!$arr) {
            throw new \Exception(__METHOD__ . " Can't get proxy from source.");
        }
        return $arr;
    }

    protected function parse($item)
    {
        return \Proxy\StringParser::create()->parse($item);
    }

    public function setIsRenewable($isRenewable)
    {
        $this->isRenewable = (bool)$isRenewable;
    }

    public function isRenewable()
    {
        return $this->isRenewable ?: false;
    }

    public function getUrl()
    {
        if (!$this->url) {
            throw new \Exception(__METHOD__ . " Invalid url");
        }
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

}
