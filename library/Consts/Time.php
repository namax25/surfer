<?php
/**
 * Date: 11/21/14
 * Time: 12:47 PM
 * @author namax
 */

namespace Consts;


class Time
{

    const MIN_IN_SEC = 60;
    const HOUR_IN_MIN = 60;
    const HOUR_IN_SEC = 3600;
    const DAY_IN_HOURS = 24;
    const DAY_IN_MIN = 1440;
    const DAY_IN_SEC = 86400;
}