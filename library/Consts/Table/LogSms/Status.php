<?php

namespace Consts\Table\LogSms;

/**
 * Description of Status
 *
 * @author namax
 */
class Status
{

    const NEWONE = 0;
    const DELIVERED = 1;
    const IN_PROGRESS = 2;
    const ERROR = 250;
    const DELETED = 255;

    static $allParams = array(
        self::NEWONE,
        self::DELIVERED,
        self::IN_PROGRESS,
        self::ERROR,
        self::DELETED,
    );

}
