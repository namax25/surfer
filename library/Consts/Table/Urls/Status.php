<?php

namespace Consts\Table\Urls;

/**
 * Description of Status
 *
 * @author namax
 */
class Status
{

    const DISABLED = 0;
    const ACTIVE = 1;
    const DELETED = 255;

    static $allParams = array(
        self::DISABLED,
        self::ACTIVE,
        self::DELETED,
    );

}
