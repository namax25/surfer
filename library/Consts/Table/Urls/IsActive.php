<?php

namespace Consts\Table\Urls;

/**
 * Description of Status
 *
 * @author namax
 */
class IsActive
{

    const DISABLED = 0;
    const ACTIVE = 1;

    static $allParams = array(
        self::DISABLED,
        self::ACTIVE,
    );

}
