<?php

namespace Consts\Table\CalendarEvents;

/**
 * Description of Status
 *
 * @author namax
 */
class HowOftenNotify
{

    const DO_NOT_REMIND = 0;
    const ONSE = 1;
    const DAYLY = 2;
    const WEEKLY = 3;
    const MONTHLY = 4;
    const EVERY_3_MONTH = 5;
    const EVERY_6_MONTH = 6;
    const YEARLY = 7;

    static $allParams = array(
        self::DO_NOT_REMIND,
        self::ONSE,
        self::DAYLY,
        self::WEEKLY,
        self::MONTHLY,
        self::EVERY_3_MONTH,
        self::EVERY_6_MONTH,
        self::YEARLY,
    );

}
