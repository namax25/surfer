<?php

namespace Consts\Table\Tariffs;

/**
 * Date: 11/25/14
 * Time: 05:30 PM
 * @author namax
 */
class Id
{


    const  STANDART = 1;
    const  EXTENDED = 2;

    static $allParams = array(
        self::STANDART,
        self::EXTENDED,
    );
} 