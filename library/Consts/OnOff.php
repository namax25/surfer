<?php

namespace Consts;

/**
 * Description of Common
 *
 * @author namax
 */
class OnOff
{

    const OFF = 0;
    const ON = 1;

    static $allParams = [
        self::OFF,
        self::ON,
    ];

}
