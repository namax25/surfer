<?php

namespace Consts;

/**
 * Description of Common
 *
 * @author namax
 */
class YesNoUnknown
{

    const UNKNOWN = 0;
    const YES = 1;
    const NO = 2;

    static $allParams = [
        self::UNKNOWN,
        self::NO,
        self::YES,
    ];


}
