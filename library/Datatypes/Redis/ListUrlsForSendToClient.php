<?php
/**
 * Date: 11/14/14
 * Time: 06:44 PM
 * @author namax
 */

namespace Datatypes\Redis;


class ListUrlsForSendToClient extends AbstractList
{

    const MAX_URLS_IN_LIST = 10000;
    const MIN_URLS_IN_LIST = 5;

    protected $key = 'list_urls_for_send';


    public function  getMaxUrlsInList()
    {
        return self::MAX_URLS_IN_LIST;
    }

    public function  getMinUrlsInList()
    {
        return self::MIN_URLS_IN_LIST;
    }
}