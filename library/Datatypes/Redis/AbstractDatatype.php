<?php
/**
 * Date: 11/14/14
 * Time: 06:44 PM
 * @author namax
 */

namespace Datatypes\Redis;


abstract class AbstractDatatype
{


    private $redis;


    /**
     * @var string
     */
    protected $key = null;


    public function __construct(\Redis $redis)
    {
        $this->redis = $redis;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return \Redis
     */
    public function getRedis()
    {
        return $this->redis;
    }


    public function delete(){
        return $this->getRedis()->del($this->getKey());
    }


} 