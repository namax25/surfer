<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 11/10/14
 * Time: 05:34 PM
 */

namespace Uitests\PageObjects;


class AddSite extends AbstractPageObject
{


    public function addNewSite(\Entities\Urls $urls)
    {

        $this->webDriver->findElement(\WebDriverBy::name("title"))->sendKeys($urls->getTitle());
        var_dump($urls->getUrl());
        $this->webDriver->findElement(\WebDriverBy::name("url"))->sendKeys($urls->getUrl());
        $this->webDriver->findElement(\WebDriverBy::name("shows_per_hour"))->sendKeys($urls->getShowsPerHour());

        $referersElement = $this->webDriver->findElement(\WebDriverBy::name("referers"));
        foreach ($urls->getReferers() as $referer) {
            $referersElement->sendKeys($referer);
            $referersElement->sendKeys(\WebDriverKeys::ENTER);
        }
        $this->webDriver->findElement(
            \WebDriverBy::cssSelector(
                '#formSiteEdit > div:nth-child(6) > div > label:nth-child(3) > input[type="radio"]'
            )
        )->click();

        $select = new \WebDriverSelect($this->webDriver->findElement(\WebDriverBy::name("time_to_show")));
        $select->selectByValue(2);
        $this->webDriver->findElement(\WebDriverBy::name("submit"))->click();
        return $this;
    }
} 