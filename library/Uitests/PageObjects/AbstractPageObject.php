<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 11/10/14
 * Time: 05:33 PM
 */

namespace Uitests\PageObjects;


abstract class AbstractPageObject
{

    protected $webDriver;

    public function __construct(\WebDriver $webDriver)
    {
        $this->webDriver = $webDriver;
    }

} 