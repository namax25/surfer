<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 11/10/14
 * Time: 05:32 PM
 */

namespace Uitests\PageObjects;


class Dashboard extends AbstractPageObject
{


    public function clickToAddNewSiteLink()
    {
        $this->webDriver->findElement(\WebDriverBy::linkText("Добавить сайт"))->click();
        return new \Uitests\PageObjects\AddSite($this->webDriver);
    }
}