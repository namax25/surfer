<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 11/10/14
 * Time: 04:13 PM
 */

namespace Uitests\PageObjects;


class LoginPage extends AbstractPageObject
{

    public function login($login, $password)
    {

        $this->webDriver->get(BASE_URL . "/auth");
        $this->webDriver->findElement(\WebDriverBy::id("inputLogin1"))->sendKeys($login);
        $this->webDriver->findElement(\WebDriverBy::id("inputEmail1"))->sendKeys($password);
        $this->webDriver->findElement(\WebDriverBy::name("submit"))->click();
        //\WebDriverKeys::ENTER
        return new \Uitests\PageObjects\Dashboard($this->webDriver);
    }

    public function getPageHeader()
    {

        $this->webDriver->get(BASE_URL . "/app/index/dashboard");
        return $this->webDriver->findElement(\WebDriverBy::xpath("/html/body/div/div[2]/h1"))->getText();
    }
} 