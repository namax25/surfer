<?php

namespace Dao;

use \Zend\Json\Json;

/**
 * Параметры
 */
class Settings extends \Dao\AbstractDao
{

    protected $table = "settings";

    public function __construct(\Zend\Db\Adapter\Adapter $db)
    {
        parent::__construct($db);
    }

    private function getSettingsTable()
    {
        return $this->tableGateway;
    }

    /**
     * Получаем параметры
     * @return type
     */
    public function getSettings()
    {
        $settings = $this->getSettingsTable()->select();
        return $this->toZendConfig($settings);
    }

    private function toZendConfig(\Zend\Db\ResultSet\ResultSet $settings)
    {
        $configArr = [];
        foreach ($settings as $setting) {
            $configArr[$setting->alias] = Json::decode($setting->params, Json::TYPE_ARRAY);
        }

        return new \Zend\Config\Config($configArr);
    }

}
