<?php

namespace Dao;

/**
 * Description of LocalDaoFabrica
 *
 * @author namax
 */
class DaoFactory implements \Zend\ServiceManager\FactoryInterface
{

    /**
     *
     * @var \Zend\Db\Adapter\Adapter
     */
    protected $db = null;

    protected $daoUsers = null;
    protected $daoUrls;
    protected $daoLogEmail;
    protected $daoReferrals;
    protected $daoBillingHistory;
    protected $daoTariff;
    protected $daoBillingType;
    protected $daoUrlsViewed;

    /**
     *
     * @var \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected $serviceLocator = null;

    public function __construct()
    {
    }


    public function createService(\Zend\ServiceManager\ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        $this->setDb($this->serviceLocator->get('db'));
        return $this;
    }

    /**
     *
     * @return \Zend\Db\Adapter\Adapter
     */
    public function getDb()
    {
        return $this->db;
    }

    public function setDb(\Zend\Db\Adapter\Adapter $db)
    {
        $this->db = $db;
    }


    protected function getDaoClass(&$variable, $className, $newDao = false)
    {
        if (is_null($variable)) {
            $variable = new $className($this->getDb());
        }
        return $newDao ? new $className($this->getDb()) : $variable;
    }


    /**
     *
     * @param bool $newDao
     * @return \Dao\Users
     */
    public function getDaoUsers($newDao = false)
    {
        if ($this->daoUsers === null) {
            $this->daoUsers = new \Dao\Users($this->getDb());
            return $this->daoUsers;
        }
        return $newDao ? new \Dao\Users($this->getDb()) : $this->daoUsers;
    }

    /**
     *
     * @param bool $newDao
     * @return \Dao\Urls
     */
    public function getDaoUrls($newDao = false)
    {
        if ($this->daoUrls === null) {
            $this->daoUrls = new \Dao\Urls($this->getDb());
            return $this->daoUrls;
        }
        return $newDao ? new \Dao\Urls($this->getDb()) : $this->daoUrls;
    }

    /**
     * @param bool $newDao
     * @return \Dao\LogEmail
     */
    public function getLogEmail($newDao = false)
    {
        return $this->getDaoClass($this->daoLogEmail, '\Dao\LogEmail', $newDao);
    }

    /**
     * @param bool $newDao
     * @return \Dao\Referrals
     */
    public function getReferrals($newDao = false)
    {
        return $this->getDaoClass($this->daoReferrals, '\Dao\Referrals', $newDao);
    }


    /**
     * @param bool $newDao
     * @return \Dao\BillingType
     */
    public function getBillingType($newDao = false)
    {
        return $this->getDaoClass($this->daoBillingType, '\Dao\BillingType', $newDao);
    }


    /**
     * @param bool $newDao
     * @return \Dao\Tariff
     */
    public function getTariff($newDao = false)
    {
        return $this->getDaoClass($this->daoTariff, '\Dao\Tariff', $newDao);
    }

    /**
     * @param bool $newDao
     * @return \Dao\BillingHistory
     */
    public function getBillingHistory($newDao = false)
    {
        return $this->getDaoClass($this->daoBillingHistory, '\Dao\BillingHistory', $newDao);
    }


    /**
     * @param bool $newDao
     * @return \Dao\UrlsViewed
     */
    public function getUrlsViewed($newDao = false)
    {
        return $this->getDaoClass($this->daoUrlsViewed, '\Dao\UrlsViewed', $newDao);
    }
}
