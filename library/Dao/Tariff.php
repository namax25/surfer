<?php
/**
 * Date: 11/25/14
 * Time: 04:05 PM
 * @author namax
 */

namespace Dao;


class Tariff extends AbstractDao
{

    protected $table = "tariffs";


    public function getAll()
    {
        $result = [];
        $tariffs = parent::getAll();
        $tariffs = $tariffs->toArray();

        foreach ($tariffs as $tariff) {
            $result[] = (new \Entities\Tariffs())->fromDb($tariff);
        }


        return $result;
    }

    /**
     * @param $id
     * @return $this|array|\ArrayObject|bool|null
     */
    public function getById($id)
    {
        $tariff =(array)parent::getById($id);
        return (new \Entities\Tariffs())->fromDb($tariff);
    }
} 