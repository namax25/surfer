<?php
/**
 * Date: 11/11/14
 * Time: 04:29 PM
 * @author namax
 */

namespace Dao;


class Referrals extends AbstractDao
{

    protected $table = "referrals";

    public function getByParentId($id)
    {
        $id = \Helpers\Strings::create()->clearDigits($id);
        return $this->getTableGateway()->select(['parent_id' => $id]);
    }


    public function getByUserId($id, $limit = 200)
    {
        $limit = (int)\Helpers\Strings::create()->clearDigits($limit);
        $id = \Helpers\Strings::create()->clearDigits($id);

        $result = $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) use ($id, $limit) {
                $select->where->
                equalTo('user_id', $id);
                $select->order('id DESC');
                $select->limit($limit);
//            echo $select->getSqlString();
            });
        return $result;
    }


    public function getByRefUserId($id, $limit = 200)
    {
        $limit = (int)\Helpers\Strings::create()->clearDigits($limit);
        $id = \Helpers\Strings::create()->clearDigits($id);

        $result = $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) use ($id, $limit) {
                $select->where->
                equalTo('ref_user_id', $id);
                $select->limit($limit);
//            echo $select->getSqlString();
            });
        return $result;
    }

} 