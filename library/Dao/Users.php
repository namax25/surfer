<?php

namespace Dao;

use  Zend\Db\Sql\Expression;

/**
 *
 * @author namax
 */
class Users extends \Dao\AbstractDao
{

    protected $table = "users";

    public function __construct(\Zend\Db\Adapter\Adapter $db)
    {
        parent::__construct($db);
    }

    public function getByLogin($login)
    {
        $login = \Helpers\Strings::create()->clear($login);
        $result = $this->getDb()->query('SELECT * FROM ' . $this->table . ' WHERE `login` = ?', [$login]);
        return $result->current();
    }


    public function addToBalance($amount, $userId)
    {
        return $this->update(
            ['balance' => new Expression('balance + ?', $amount)], ['id' => $userId]
        );
    }

}
