<?php

namespace Dao;

/**
 *
 * @author namax
 */
class Urls extends \Dao\AbstractDao
{

    protected $table = "urls";

    public function __construct(\Zend\Db\Adapter\Adapter $db)
    {
        parent::__construct($db);
    }

    public function getByIdForUser($userId, $id)
    {

        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) use ($userId, $id) {
            $select->where->
            equalTo('user_id', $userId)->
            equalTo('id', $id);
//            echo $select->getSqlString();
        });

        return $result->current();
    }

    public function getUrlsFromId($id, $limit = 200)
    {
        $limit = (int)\Helpers\Strings::create()->clearDigits($limit);
        $id = \Helpers\Strings::create()->clearDigits($id);

        $result = $this->getTableGateway()->select(
            function (\Zend\Db\Sql\Select $select) use ($id, $limit) {
                //$select->columns(['id', 'ad_id', 'uri', 'phone_download_count', 'region']);
                $select->where->greaterThan('id', $id)->equalTo('is_active', \Consts\Table\Urls\IsActive::ACTIVE);
                $select->limit($limit);
//            echo $select->getSqlString();
            });
        return $result;
    }


    public function getLastUrl()
    {

        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) {
            $select->order('id desc')->limit(1);
        });

        return $result->current();
    }


    public function getFirstUrl()
    {

        $result = $this->getTableGateway()->select(function (\Zend\Db\Sql\Select $select) {
            $select->limit(1);
        });

        return $result->current();
    }
}
