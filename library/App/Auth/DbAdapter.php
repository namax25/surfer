<?php

namespace App\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Adapter\AbstractAdapter;
use Zend\Authentication\Result as AuthenticationResult;

/**
 * DbAdapter
 *
 * @author namax
 *
 */
class DbAdapter extends AbstractAdapter implements AdapterInterface
{

    private $userDao = null;

    public function __construct(\Dao\Users $userDao)
    {
        $this->userDao = $userDao;
    }

    public function authenticate()
    {

        $userData = $this->userDao->getByLogin($this->getIdentity());
        $this->sesscontainer = new \Zend\Session\Container('sess_acl');

        $pass = $this->getPassword($userData->login, $this->getCredential());

        if (!empty($userData->password) && password_verify($pass, $userData->password)) {
            $this->sesscontainer->userData = $userData;
            return new AuthenticationResult(1, $userData);
        }
        return new AuthenticationResult(0, $userData, ['ACCESS DENIED']);
    }

    public function generateNewHash($userData, $password)
    {
        return \Helpers\Password::create()->generate($userData->login, $password);
    }

    public function isValidLoginPass($login, $password, $id)
    {
        $userData = $this->userDao->getByLogin($login);
        return password_verify($this->getPassword($login, $password, $id), $userData->password);
    }

    protected function getPassword($login, $password)
    {
        return $password . $login;
    }
}
