<?php

namespace App\Controllers\Responses;

/**
 * Description of JsonResponse
 *
 * @author namax
 */
class JsonResponse implements \JsonSerializable, \IteratorAggregate
{

    private $payload = null;
    private $errors = [];
    private $debugMessages = [];
    private $infoMessages = [];
    private $warnMessages = [];
    private $errMessages = [];
    private $successMessages = [];
    private $success = false;

    public function __construct($payload = [], array $errors = [])
    {
        $this->payload = $payload;
        $this->errMessages = $errors;
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->toArray());
    }

    public function toArray()
    {
        $this->success = $this->hasErrors();

        $data = [];

        $data['success'] = $this->success;
        $data['msg'] = $this->getAllMessages();

        if ($this->success) {
            $data['payload'] = $this->payload;
        }

        return $data;
    }

    protected function hasErrors()
    {
        return empty($this->warnMessages) && empty($this->errMessages);
    }

    protected function getAllMessages()
    {
        $this->errors = [];
        if ($this->debugMessages) {
            $this->errors['debug'] = $this->debugMessages;
        }
        if ($this->infoMessages) {
            $this->errors['info'] = $this->infoMessages;
        }
        if ($this->warnMessages) {
            $this->errors['warn'] = $this->warnMessages;
        }
        if ($this->errMessages) {
            $this->errors['errors'] = $this->errMessages;
        }
        if ($this->successMessages) {
            $this->errors['success'] = $this->successMessages;
        }
        return $this->errors;
    }

    public function addDebug($debugMessages)
    {
        $this->addMessages($this->debugMessages, $debugMessages);
    }

    public function addInfo($infoMessages)
    {
        $this->addMessages($this->infoMessages, $infoMessages);
    }

    public function addWarn($warnMessages)
    {
        $this->addMessages($this->warnMessages, $warnMessages);
    }

    public function addSuccess($successMessages)
    {
        $this->addMessages($this->successMessages, $successMessages);
    }

    public function addErr($errMessages)
    {
        $this->addMessages($this->errMessages, $errMessages);
    }

    protected function addMessages(&$messageArr, $messages)
    {
        if (is_array($messages)) {
            foreach ($messages as $msg) {
                $messageArr[] = (string)$msg;
            }
        } else {
            $messageArr[] = (string)$messages;
        }
    }

    public function setDebugMessages(array $debugMessages)
    {
        $this->debugMessages = $debugMessages;
    }

    public function clearAllMessages()
    {
        $this->debugMessages = [];
        $this->infoMessages = [];
        $this->warnMessages = [];
        $this->errMessages = [];
        $this->successMessages = [];
    }

    public function clearPayload()
    {
        $this->payload = [];
    }

    public function clearAll()
    {
        $this->clearPayload();
        $this->clearAllMessages();
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;
    }

}
