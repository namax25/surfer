<?php
/**
 * Date: 11/20/14
 * Time: 05:45 PM
 * @author namax
 */

namespace App\Controllers\Responses;


class JsonStringModel extends \Zend\View\Model\JsonModel
{

    protected $jsonString = null;

    public function __construct($jsonString)
    {

        parent::__construct();

        $this->jsonString = $jsonString ? $jsonString : '[]';
    }

    public function serialize()
    {
        return $this->jsonString;
    }
} 