<?php

namespace Helpers;

/**
 * Description of Password
 *
 * @author namax
 */
class Password extends \AbstractHelper
{

    public function generate($login, $password)
    {
        $options = [
            'cost' => 11,
            'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
        ];
        $pass = $password . $login;

        return password_hash($pass, PASSWORD_BCRYPT, $options);
    }

    public function getRandomPassword($lenght = 8)
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $alphabet = str_shuffle($alphabet);
        $pass = [];
        $alphaLength = strlen($alphabet) - 1;

        for ($i = 0; $i < $lenght; $i++) {
            $n = mt_rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }

        return implode($pass);
    }
}
