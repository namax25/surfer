<?php

namespace Helpers;

/**
 * Description of Ads
 * Помощник для работы с объявлениями
 * @author namax
 */
class Strings extends \AbstractHelper
{

    /**
     * Проверка содержит ли строка заданную подстроку
     * @param $substring - подстрока
     * @param $string - строка
     * @param bool $utf - строка в кодировки utf-8?
     * @param bool $caseInsensitive - учитывать регистр
     * @return bool
     */
    public function isStrContain($substring, $string, $utf = true, $caseInsensitive = true)
    {
        if ($utf) {
            $pos = $caseInsensitive ? mb_stripos($string, $substring, 0, "UTF-8") :
                mb_strpos($string, $substring, 0, "UTF-8");
        } else {
            $pos = $caseInsensitive ? stripos($string, $substring) : strpos($string, $substring);
        }
        return $pos === false ? false : true;
    }

    /**
     * Убирает все не нужные пробелы
     * @param $string $string - исходная строка
     * @return string строка без пробелов
     */
    public function htmlTrim($string)
    {
        $pattern = '([ \s\t\n\r\x0B\x00\x{A0}\x{AD}\x{2000}-\x{200F}\x{201F}\x{202F}\x{3000}\x{FEFF}]' .
            '|&nbsp;|&#160;|<br\s*\/?>)+';
        return trim(preg_replace('/' . $pattern . '|' . $pattern . '/u', ' ', $string));
    }

    public function clear($string)
    {
        return $this->clearTemplate(
            $string,
            $this->commonReplaceFunc("/[^a-zA-Z0-9а-яА-ЯёЁ\_\=\ \{\}\[\]\(\)\,\.\!\?\@\*\+\-\;\:\|\/\#]+/iu")
        );
    }

    public function clearUrl($string)
    {
        $url = trim($string, '!"#$%&\'()*+,-./@:;<=>[\\]^_`{|}~');

        if ($url !== 'http' && !preg_match("/http[s]?\:\/\//", $url)) {
            $url = "http://" . $url;
        }

        $pattern = "/^([a-z][a-z0-9\*\-\.]*):\/\/(?:(?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*" .
            "(?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@)?(?:(?:[a-z0-9\-\.]|%[0-9a-f]{2})+|" .
            "(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\]))(?::[0-9]+)?" .
            "(?:[\/|\?](?:[\w#!:\.\?\+=&@!$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})*)?$/xi";
        return preg_match($pattern, $url) ? $url : false;

    }

    public function clearAlias($string)
    {

        return $this->clearTemplate($string, function ($string) {
            $string = mb_strtolower($string);
            return preg_replace("/[^a-zA-Z0-9\_]+/iu", "", $string);
        });
    }


    public function clearHash($string)
    {

        return $this->clearTemplate($string, function ($string) {
            $string = mb_strtolower($string);
            return preg_replace("/[^a-z0-9]+/iu", "", $string);
        });
    }

    public function clearFloat($string)
    {
        return $this->clearTemplate($string, $this->commonReplaceFunc("/[^0-9\.]+/u"));
    }


    public function clearDigits($string)
    {
        return $this->clearTemplate($string, $this->commonReplaceFunc("/\D/u"));
    }

    public function clearId($string)
    {
        $id = $this->clearTemplate($string, $this->commonReplaceFunc("/\D/u"));
        if (!is_numeric($id)) {
            throw new \InvalidArgumentException(__METHOD__ . " Invalid id");
        }
        return $id;
    }

    /**
     * Первая буква заглавная
     * @param $str
     * @param bool $utf
     * @return string
     */
    public function ucfirst($str, $utf = true)
    {
        if ($utf) {
            $firstCharacter = mb_strtoupper(mb_substr($str, 0, 1, "UTF-8"), "UTF-8");
            $len = mb_strlen($str) - 1;
            return $firstCharacter . mb_substr($str, 1, $len, "UTF-8");
        } else {
            return ucfirst($str);
        }
    }

    /**
     * Шаблон для работы с массивами и строками
     * @param $string
     * @param callable $func
     * @return array|bool
     */
    private function clearTemplate($string, $func)
    {
        if (!is_callable($func)) {
            return false;
        }
        if (is_array($string)) {
            array_walk($string, function (&$el) use ($func) {
                $el = $func($el);
            });
            return $string;
        } else {
            return $func($string);
        }
    }

    /**
     * Обшая функция замены
     * @param $pattern
     * @return callable
     */
    private function commonReplaceFunc($pattern)
    {
        return function ($string) use ($pattern) {
            return preg_replace($pattern, "", $string);
        };
    }
}
