<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Log;

use Zend\Log\Exception;
use  Zend\Stdlib;

/**
 * Logging messages with a stack of backends
 */
class Logger extends \Zend\Log\Logger
{

    /**
     * @const int defined from the BSD Syslog message severities
     * @link http://tools.ietf.org/html/rfc3164
     */
//    const EMERG = 0;
//    const ALERT = 1;
//    const CRIT = 2;
//    const ERR = 3;
//    const WARN = 4;
//    const NOTICE = 5;
//    const INFO = 6;
//    const DEBUG = 7;

    const OK = 101;  // Debug: debug messages
    const DONE = 102;  // Debug: debug messages
    const START = 103;  // Debug: debug messages
    const END = 104;  // Debug: debug messages
    const STATUS = 105;  // Debug: debug messages

    /**
     * List of priority code => priority (short) name
     *
     * @var array
     */

    protected $priorities = array(
        self::EMERG => 'EMERG',
        self::ALERT => 'ALERT',
        self::CRIT => 'CRIT',
        self::ERR => 'ERR',
        self::WARN => 'WARN',
        self::NOTICE => 'NOTICE',
        self::INFO => 'INFO',
        self::OK => 'OK',
        self::DONE => 'DONE',
        self::START => 'START',
        self::END => 'END',
        self::STATUS => 'STATUS',
    );

    /**
     * Add a message as a log entry
     *
     * @param  int $priority
     * @param  mixed $message
     * @param  array|\Traversable $extra
     * @return Logger
     * @throws Exception\InvalidArgumentException if message can't be cast to string
     * @throws Exception\InvalidArgumentException if extra can't be iterated over
     * @throws Exception\RuntimeException if no log writer specified
     */
    public function log($priority, $message, $extra = array())
    {
        if (!is_int($priority) || ($priority < 0) || !array_key_exists($priority, $this->priorities)) {
            throw new Exception\InvalidArgumentException(sprintf(
                '$priority must be an integer > 0 and < %d; received %s', count($this->priorities),
                var_export($priority, 1)
            ));
        }
        if (is_object($message) && !method_exists($message, '__toString')) {
            throw new Exception\InvalidArgumentException(
                '$message must implement magic __toString() method'
            );
        }

        if (!is_array($extra) && !$extra instanceof \Traversable) {
            throw new Exception\InvalidArgumentException(
                '$extra must be an array or implement \Traversable'
            );
        } elseif ($extra instanceof \Traversable) {
            $extra = \Zend\Stdlib\ArrayUtils::iteratorToArray($extra);
        }

        if ($this->writers->count() === 0) {
            throw new Exception\RuntimeException('No log writer specified');
        }

        $timestamp = date("Y-m-d H:i:s");

        if (is_array($message)) {
            $message = var_export($message, true);
        }

        $event = array(
            'timestamp' => $timestamp,
            'priority' => (int)$priority,
            'priorityName' => $this->priorities[$priority],
            'extra' => $extra
        );
        if ($message instanceof \Exception) {
            $event['message'] = $message->getMessage();
            $event['trace'] = $message->getTraceAsString();
        } else {
            $event['message'] = (string)$message;
        }

        foreach ($this->processors->toArray() as $processor) {
            $event = $processor->process($event);
        }

        foreach ($this->writers->toArray() as $writer) {
            $writer->write($event);
        }

        return $this;
    }
}