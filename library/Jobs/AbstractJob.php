<?php

namespace Jobs;

/**
 * Description of AbstractJob
 *
 * @author namax
 */
abstract class AbstractJob
{

    protected $serviceManager;
    protected $logCli;
    protected $logDb;
    protected $daoJobs;
    protected $jobName;
    protected $daoFactory;
    protected $redis;

    /**
     * Данные "как есть", без распарсевания json
     * @var type
     */
    protected $jobRawData;

    /**
     *
     * @var \Zend\Config\Config
     */
    protected $settings;

    /**
     * id для конкретного объявления\записи в базе. Периодически надо протестировать на какой-то одной записи
     * @var null
     */
    protected $idStatic = null;

    final public function __construct(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager, $jobName)
    {
        $this->setServiceManager($serviceManager);
        $this->jobName = $jobName;

        //обязательно должно быть в конструкторе для возможнсти 
        //получения параметров в пусковых файлах
        $this->initSettings();
    }

    public function startTheJob()
    {

        try {
            $this->updateStartDate($this->getJobRawData()->id);
            $startTime = microtime(true);
            $this->configure();
            $this->doAll();
            $endTime = round((microtime(true) - $startTime), 4);
            $this->getLogCli()->log(\Log\Logger::DONE, "SUCCESS. Elapsed time: " . $endTime);
            $this->updateElapsedTime($this->getJobRawData()->id, $endTime);
        } catch (\Exception $ex) {
            $this->getLogCli()->err($ex);
            $this->getLogDb()->err($ex,
                ['id' => $this->getJobRawData()->id, "job_name" => $this->getJobRawData()->name]);
        }
    }


    /**
     * @return \Dao\DaoFactory
     */
    protected function getDaoFactory()
    {
        if ($this->daoFactory === null) {
            $this->daoFactory = $this->getServiceManager()->get('DaoFactory');
        }
        return $this->daoFactory;
    }

    /**
     * @return \Redis
     */
    protected function getRedis()
    {
        if ($this->redis === null) {
            $this->redis = $this->getServiceManager()->get('Redis');
        }
        return $this->redis;
    }

    protected function updateSettings(\Zend\Config\Config $settings = null)
    {
        if (is_null($settings)) {
            $settings = $this->getSettings();
        }

        $settingsInJson = \Zend\Json\Json::encode($settings->toArray());

        $this->getDaoJobs()->update(
            ['params' => $settingsInJson],
            ['id' => $this->getJobRawData()->id]
        );
    }

    /**
     *
     * @return \Zend\Config\Config
     */
    public function getSettings()
    {
        return $this->settings;
    }

    public function setSettings(\Zend\Config\Config $settings)
    {
        $this->settings = $settings;
    }

    private function initSettings()
    {
        $this->jobRawData = $this->getDaoJobs()->getByName($this->jobName);
        if (!$this->jobRawData['published']) {
            throw new \Exception(__METHOD__ . " The Job is off. Published = 0");
        }
        $jsonReader = new \Zend\Config\Reader\Json();
        $this->settings = new \Zend\Config\Config($jsonReader->fromString($this->jobRawData['params']), true);
    }

    protected abstract function configure();

    protected abstract function doAll();

    protected function getServiceManager()
    {
        return $this->serviceManager;
    }

    protected function setServiceManager(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    /**
     *
     * @return \Zend\Log\LoggerInterface
     */
    protected function getLogCli()
    {
        if ($this->logCli === null) {
            return $this->getServiceManager()->get('log_cli');
        }
        return $this->logCli;
    }

    /**
     *
     * @return \Zend\Log\LoggerInterface
     */
    protected function getLogDb()
    {
        if ($this->logDb === null) {
            return $this->getServiceManager()->get('log_db');
        }
        return $this->logDb;
    }

    protected function setLogCli(\Zend\Log\LoggerInterface $logCli)
    {
        $this->logCli = $logCli;
    }

    protected function setLogDb(\Zend\Log\LoggerInterface $logDb)
    {
        $this->logDb = $logDb;
    }

    /**
     *
     * @return \Dao\Jobs
     */
    protected function getDaoJobs()
    {
        if ($this->daoJobs === null) {
            $this->daoJobs = new \Dao\Jobs($this->getDb());
        }
        return $this->daoJobs;
    }

    protected function setDaoGrabbers($daoGrabbers)
    {
        $this->daoJobs = $daoGrabbers;
    }

    protected function getDb()
    {
        return $this->getServiceManager()->get('db');
    }

    /**
     * @return \Zend\ServiceManager\Config
     */
    protected function getGlobalConfigs()
    {
        return $this->getServiceManager()->get('globalConfig');
    }

    protected function getJobRawData()
    {
        return $this->jobRawData;
    }

    protected function updateStartDate($id)
    {
        $this->getDaoJobs()->update(['last_active_date' => \Helpers\DateTime::create()->now()], ['id' => $id]);
    }

    protected function updateElapsedTime($id, $time)
    {
        $this->getDaoJobs()->update(['last_elapsed_time' => $time], ['id' => $id]);
    }

    /**
     * @return null
     */
    public function getIdStatic()
    {
        return $this->idStatic;
    }

    /**
     * @param null $idStatic
     */
    public function setIdStatic($idStatic)
    {

        $idStatic = \Helpers\Strings::create()->clearDigits($idStatic);

        if ($idStatic) {
            $this->idStatic = $idStatic;
        }
    }

}
