<?php
/**
 * Date: 11/13/14
 * Time: 04:56 PM
 * @author namax
 */

namespace Jobs;

use \Datatypes\Redis;
use \Zend\Json\Json;
use \Helpers\DateTime;

class FillUrlQueue extends AbstractJob
{

    private $maxIdInDb = 0;
    private $firstIdInDb = 0;
    private $currentId = 0;

    const STEP = 2;
    const TIME_CICLE_PERIOD_MIN = 60;

    /**
     * @var \Datatypes\Redis\ListUrlsForSendToClient
     */
    protected $listUrlsForSendToClient = null;

    protected function configure()
    {
        $k = $this->getRedis();
        $this->listUrlsForSendToClient = new Redis\ListUrlsForSendToClient($this->getRedis());
        $this->currentId = (int)$this->getSettings()->last_id;
    }

    protected function doAll()
    {


        if ($this->isListHasEnoughtUrls()) {

            $this->getLogCli()->info("List has enough items " . $this->listUrlsForSendToClient->count());
            return;
        }

        $urls = $this->getUrls($this->currentId, self::STEP);

        if ($urls->count() == 0) {
            $this->getSettings()->last_id = $this->getFirstIdInDb() - 1;
            $this->updateSettings();
            $this->getLogCli()->info("No urls were wound. On next turn we start from the begining");
            return;
        }

        $list = [];
        $addedItemsCounter = 0;;

        foreach ($urls as $url) {
            $this->currentId = $url['id'];
            $referer = $this->selectRandomReferer(Json::decode($url['referers']));
            $urlDate = $url['next_active_date'] ? DateTime::create()->getDateTimeObject($url['next_active_date']) : null;

            if ($urlDate && $urlDate > DateTime::create()->getDateTimeObject()) {
                //$this->getLogCli()->info("Passed url id " . $url['id'] . ". Not allowed by date ");
                continue;
            }

            $urlForRedis = $url;
            $urlForRedis['referers'] = $referer;
            $list[] = Json::encode($urlForRedis);

            $amountUrlsToAdd = $this->calculateAmountUrlsToAdd($url);
            for ($i = 0; $i < $amountUrlsToAdd; $i++) {
                $addedItemsCounter++;
                $this->listUrlsForSendToClient->prepend(Json::encode($urlForRedis));
            }

            $this->updateNextActiveTime($url);

        }

        var_dump($list, $this->currentId);
        $this->getSettings()->last_id = $this->currentId;
        $this->updateSettings();

        $this->getLogCli()->info("Elements in the list: " . $this->listUrlsForSendToClient->count());


       /* while ($this->listUrlsForSendToClient->count() > 0) {
            var_dump($this->listUrlsForSendToClient->rPop());
        }
        $this->getLogCli()->info("Elements in the list: " . $this->listUrlsForSendToClient->count());*/

    }

    private function updateNextActiveTime($url)
    {

        $date = \Helpers\DateTime::create()->getDateTimeObject();

        $minute = $this->calculateMinutesForNextActiveTime($url);
        $date->add(new \DateInterval("PT" . $minute . "M"));
        $this->getDaoFactory()->getDaoUrls()->update(
            ['next_active_date' => \Helpers\DateTime::create()->printDate($date)], ['id' => $url['id']]
        );

    }

    private function calculateMinutesForNextActiveTime($url)
    {

        return $url['shows_per_hour'] < self::TIME_CICLE_PERIOD_MIN ?
            (int)ceil(self::TIME_CICLE_PERIOD_MIN / $url['shows_per_hour']) : 1;

    }

    private function calculateAmountUrlsToAdd($url)
    {

        return $url['shows_per_hour'] > self::TIME_CICLE_PERIOD_MIN ?
            (int)ceil($url['shows_per_hour'] / self::TIME_CICLE_PERIOD_MIN) : 1;

    }

    private function isListHasEnoughtUrls()
    {
        $countList = $this->listUrlsForSendToClient->count();
        return $countList > $this->listUrlsForSendToClient->getMinUrlsInList() ||
        $countList > $this->listUrlsForSendToClient->getMaxUrlsInList();
    }

    private function selectRandomReferer($referes)
    {
        if (empty($referes) || !is_array($referes)) {
            return null;
        }

        return $referes[array_rand($referes, 1)];
    }

    private function getNextId()
    {

        $nextId = 0;
        $lastId = (int)$this->getSettings()->last_id;


        if ($lastId <= 0) {
            $nextId = $this->getFirstIdInDB();
        } else {

            $lastId++;

            if ($lastId > $this->getMaxIdInDb()) {
                $nextId = $this->getFirstIdInDB();
            }
            $nextId = $lastId;
        }

        return $nextId;
    }


    private function getUrl($id)
    {
        return $this->getDaoFactory()->getDaoUrls()->getById($id);
    }

    private function getUrls($id, $limit)
    {
        return $this->getDaoFactory()->getDaoUrls()->getUrlsFromId($id, $limit);
    }


    /**
     * @return int
     * @throws \Exception
     */
    public function getFirstIdInDb()
    {

        if ($this->firstIdInDb == 0) {
            $firstUrl = $this->getDaoFactory()->getDaoUrls()->getFirstUrl();

            if (!$firstUrl) {
                throw new \Exception(__METHOD__ . " First url not found. Maybe the table is empty?");
            }

            $this->firstIdInDb = $firstUrl['id'];
        }
        return $this->firstIdInDb;
    }

    /**
     * @return int
     * @throws \Exception
     */
    public function getMaxIdInDb()
    {
        if ($this->maxIdInDb == 0) {
            $lastUrl = $this->getDaoFactory()->getDaoUrls()->getLastUrl();

            if (!$lastUrl) {
                throw new \Exception(__METHOD__ . " Last url not found. Maybe the table is empty?");
            }

            $this->maxIdInDb = $lastUrl['id'];

        }
        return $this->maxIdInDb;
    }


}