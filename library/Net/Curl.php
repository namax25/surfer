<?php

namespace Net;

use Zend\Config\Config;

/**
 * Description of MultiCurl
 *
 * @author namax
 */
class Curl
{

    private $cookiePath = null;
    private $cookieFileName = null;
    private $curlOptions = [];
    private $proxyManger = null;

    /**
     *
     * @var \Entity\Proxy
     */
    private $lastProxy = null;

    /**
     * Очередь урл для скачивания
     * @var \SplQueue
     */
    private $urlsQueue = null;

    public function get($url, $callback)
    {
        $this->addOptions([
            CURLOPT_RETURNTRANSFER => true,
        ]);
        $this->makeRequest($url, $callback);
    }

    public function post($url, $postFields, $callback)
    {
        $this->addOptions([
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postFields,
            CURLOPT_RETURNTRANSFER => true,
        ]);

        $this->makeRequest($url, $callback);
    }

    public function getFile($url, $pathToFile, $callback)
    {
        $fp = fopen($pathToFile, 'w');
        $this->addOptions([
            CURLOPT_FILE => $fp,
        ]);
        $this->makeRequest($url, $callback);
        fclose($fp);
        $this->delOption(CURLOPT_FILE);
    }

    public function run(\Iterator $urls, $callback)
    {
        $this->getUrlsQueue();

        $this->addOptions([
            CURLOPT_RETURNTRANSFER => true,
        ]);
        while ($urls->valid()) {
            $this->makeRequest($urls->current(), $callback);
            $urls->next();
        }

        while (!$this->getUrlsQueue()->isEmpty()) {
            $this->makeRequest($this->getUrlsQueue()->dequeue(), $callback);
        }
        return true;
    }

    protected function makeRequest($url, $callback)
    {
        if (!is_callable($callback)) {
            throw new \Exception(__METHOD__ . " callback not a function");
        }

        $ch = $this->prepareHandler($url);
        $response = curl_exec($ch);
        $error = curl_error($ch);
        $info = curl_getinfo($ch);
        $info['error_code'] = curl_errno($ch);
        $info['origin_url'] = $url;

        //нельзя ставить после callback т.к. cookie file создается только после закрытия
        curl_close($ch);

        if ($this->getProxyManger()) {
            $this->getProxyManger()->setLastError($this->getLastProxy(), $info['http_code'], $error);
            $this->clearLastProxy();
        }

        $code = $callback($info, $response, $error);
        $this->codeHandler($code, $url);
    }

    /**
     * 0 - success , can take next url
     * 1 - return url to queue
     * 255 - stop code
     * @param type $code
     */
    protected function codeHandler($code, $url)
    {
        switch ($code) {
            case Consts\CurlControlStatus::PUT_URL_BACK_TO_QUEUE:
                $this->getUrlsQueue()->enqueue($url);
                break;
        }
    }

    protected function prepareHandler($url)
    {

        $ch = curl_init();

        $options = [];
        $options[CURLOPT_URL] = trim($url);

        $this->setCommonOptions($options);
        $this->setCookieOptions($options);
        $this->setProxyOptions($options, $ch);

        if (is_array($this->getOptions())) {
            foreach ($this->getOptions() as $key => $option) {
                $options[$key] = $option;
            }
        }
        var_dump($options);
        curl_setopt_array($ch, $options);
        return $ch;
    }

//<editor-fold defaultstate="collapsed" desc="Set Oprions For Curl Handler">
    protected function setCommonOptions(&$options)
    {
//        $options[CURLOPT_USERAGENT] = $this->getRandomUserAgent();
        $options[CURLOPT_HEADER] = false;
        $options[CURLOPT_FOLLOWLOCATION] = true;
    }

    protected function setProxyOptions(&$options)
    {
        if ($this->getProxyManger()) {
            $proxy = $this->getProxyManger()->getProxy();
            if ($proxy == null) {
                throw new \Exception("Not enough proxy");
            }
            $this->setLastProxy($proxy);
//            echo $proxy->getUrl() . ":" . $proxy->getPort() . "\n";
            $options[CURLOPT_PROXY] = $proxy->getUrl() . ":" . $proxy->getPort();
//            $options[CURLOPT_PROXY] = "194.58.46.194:13128";
            if ($proxy->getLogin()) {
                $options[CURLOPT_PROXYUSERPWD] = $proxy->getLogin() . ":" . $proxy->getPassword();
                $options[CURLOPT_PROXYAUTH] = 1;
            }
//            var_dump($options);
        }
    }

    protected function setCookieOptions(&$options)
    {

        if ($this->getCookiePath()) {

            if ($this->getCookieFileName()) {
                $cookieFileName = "cookie_" . $this->getCookieFileName();
                $options[CURLOPT_COOKIEFILE] = $this->getCookiePath() . DIRECTORY_SEPARATOR . $this->getCookieFileName() . '.txt';
                $options[CURLOPT_COOKIEJAR] = $this->getCookiePath() . DIRECTORY_SEPARATOR . $this->getCookieFileName() . '.txt';
            } else {
                $cookieFileName = "cookie_" . time() . mt_rand();
                $options[CURLOPT_COOKIEFILE] = $this->getCookiePath() . DIRECTORY_SEPARATOR . $cookieFileName . '.txt';
                $options[CURLOPT_COOKIEJAR] = $this->getCookiePath() . DIRECTORY_SEPARATOR . $cookieFileName . '.txt';
            }
        }
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Getters\Setters">

    public function getLastProxy()
    {
        return $this->lastProxy;
    }

    public function setLastProxy(\Entity\Proxy $lastProxy)
    {
        $this->lastProxy = $lastProxy;
    }

    public function clearLastProxy()
    {
        $this->lastProxy = null;
    }

    public function getUrlsQueue()
    {
        if (!$this->urlsQueue) {
            $this->urlsQueue = new \SplQueue();
        }
        return $this->urlsQueue;
    }

    public function setOptions($options)
    {
        $this->curlOptions = $options;
    }

    public function addOptions($options)
    {
        if (is_array($options)) {
            foreach ($options as $key => $option) {
                $this->curlOptions[$key] = $option;
            }
        }
    }

    public function delOption($option)
    {
        if (isset($this->curlOptions[$option])) {
            unset($this->curlOptions[$option]);
        }
    }

    public function getOptions()
    {
        return $this->curlOptions;
    }

    public function getStopFlag()
    {
        return $this->stopFlag;
    }

    public function setStopFlag($stopFlag)
    {
        $this->stopFlag = $stopFlag;
    }

    public function getCookiePath()
    {
        return $this->cookiePath;
    }

    public function setCookiePath($cookiePath)
    {
        if ($cookiePath === false) {
            throw new \Exception(__METHOD__ . " Path to cookies does not exist " . $cookiePath);
        }
        $this->cookiePath = $cookiePath;
    }

    public function getCookieFileName()
    {
        return $this->cookieFileName;
    }

    public function setCookieFileName($cookieFileName)
    {
        $this->cookieFileName = $cookieFileName;
    }

    /**
     *
     * @return \Proxy\Manager\ProxyManagerInterface
     */
    public function getProxyManger()
    {
        return $this->proxyManger;
    }

    public function setProxyManger(\Proxy\Manager\ProxyManagerInterface $proxyManger)
    {
        $this->proxyManger = $proxyManger;
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Utils">

    protected function getRandomUserAgent()
    {
        return \Net\Helpers\UserAgents::create()->getRandom();
    }

    /**
     * Clean cookies storage
     */
    public function cleanCookiesStorage()
    {
        if ($this->getCookiePath() && file_exists($this->getCookiePath())) {
            $itr = new \DirectoryIterator($this->getCookiePath());
            foreach ($itr as $item) {
                if (!$item->isFile() || $item->getExtension() != 'txt' || !preg_match("/^cookie_\d+\.txt/",
                        $item->getFilename())
                ) {
                    continue;
                }
                unlink($item->getPathname());
            }
        }
    }
// </editor-fold>
}
