<?php

namespace Sms;

/**
 *
 * @author namax
 */
interface iSms
{

    public function send($fromPhoneNumber, $toPhoneNumber, $text);
}
