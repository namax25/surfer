<?php

namespace Collections;

/**
 *
 * @author namax
 */
interface ObjectInterface
{

    public function hashCode();

    public function toArray();

    public function fromArray(array $data);
}
