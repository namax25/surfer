<?php
/**
 * User: namax
 * Date: 09/10/14
 * Time: 04:18 PM
 */
namespace Entities;

use \Zend\Json\Json;

class Urls extends AbstractEntity
{
    const MAX_AMOUNT_OF_REFERERS = 80;

    protected $id = null;
    protected $url = null;
    protected $userId = null;
    protected $title = null;
    protected $timeToShow = null;
    protected $showInterval = null;
    protected $showsPerHour = null;
    protected $status = null;
    protected $isActive = null;
    protected $referers = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['url'])) {
            $this->setUrl($data['url']);
        }
        if (isset($data['user_id'])) {
            $this->setUserId($data['user_id']);
        }
        if (isset($data['title'])) {
            $this->setTitle($data['title']);
        }
        if (isset($data['time_to_show'])) {
            $this->setTimeToShow($data['time_to_show']);
        }
        if (isset($data['show_interval'])) {
            $this->setShowInterval($data['show_interval']);
        }
        if (isset($data['shows_per_hour'])) {
            $this->setShowsPerHour($data['shows_per_hour']);
        }
        if (isset($data['status'])) {
            $this->setStatus($data['status']);
        }
        if (isset($data['referers'])) {
            $referers = $data['referers'];
            if (!is_array($data['referers'])) {

                $referers = explode("\r\n", $data['referers']);
            }

            $this->setReferers($referers);
        }
        if (isset($data['status'])) {
            $this->setStatus($data['status']);

        }
        if (isset($data['is_active'])) {
            $this->setIsActive($data['is_active']);
        }
    }

    public function clear()
    {
        $this->id = null;
        $this->url = null;
        $this->userId = null;
        $this->title = null;
        $this->timeToShow = null;
        $this->showInterval = null;
        $this->showsPerHour = null;
        $this->status = null;
        $this->isActive = null;
        $this->referers = null;
        return $this;
    }


    public function toArray()
    {
        $data = [];

        $data['id'] = $this->getId();
        $data['url'] = $this->getUrl();
        $data['user_id'] = $this->getUserId();
        $data['title'] = $this->getTitle();
        $data['time_to_show'] = $this->getTimeToShow();
        $data['show_interval'] = $this->getShowInterval();
        $data['shows_per_hour'] = $this->getShowsPerHour();
        $data['status'] = $this->getStatus();
        $data['is_active'] = $this->getIsActive();
        $data['referers'] = $this->getReferers();
        return $data;
    }

    public function toDb()
    {
        $data = $this->toArrayNoNull();

        if (isset($data['referers'])) {
            $data['referers'] = Json::encode($data['referers']);
        }

        return $data;
    }

    public function fromDb($data)
    {
        if (isset($data['referers'])) {
            $data['referers'] = Json::decode($data['referers'], Json::TYPE_ARRAY);
        }
        $this->fromArray($data);
        return $this;
    }


    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = \Helpers\Strings::create()->clearId($id);
    }

    /**
     * @return null
     */
    public function getReferers()
    {
        return $this->referers;
    }


    /**
     * @param array $referers
     */
    public function setReferers(array $referers)
    {
        $result = [];
        foreach ($referers as $referer) {
            $referer = \Helpers\Strings::create()->clearUrl($referer);
            if ($this->isRefererClearAndAmountNotOverLimit($referer, count($result))) {
                $result[] = $referer;
            }
        }
        $this->referers = $result;
    }

    private function isRefererClearAndAmountNotOverLimit($referer, $amountOfClearedReferers)
    {
        return $referer && $amountOfClearedReferers <= self::MAX_AMOUNT_OF_REFERERS;
    }

    /**
     * @return null
     */
    public function getShowInterval()
    {
        return $this->showInterval;
    }

    /**
     * @param null $showInterval
     */
    public function setShowInterval($showInterval)
    {
        $showInterval = \Helpers\Strings::create()->clearDigits($showInterval);
        if ($showInterval) {
            $this->showInterval = $showInterval;
        }

    }

    /**
     * @return null
     */
    public function getShowsPerHour()
    {
        return $this->showsPerHour;
    }

    /**
     * @param null $showsPerHour
     */
    public function setShowsPerHour($showsPerHour)
    {
        $showsPerHour = \Helpers\Strings::create()->clearDigits($showsPerHour);
        if ($showsPerHour) {
            $this->showsPerHour = $showsPerHour;
        }

    }

    /**
     * @return null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param null $status
     */
    public function setStatus($status)
    {
        $status = \Helpers\Strings::create()->clearDigits($status);
        if (in_array($status, \Consts\Table\Urls\Status::$allParams)) {
            $this->status = $status;
        }
    }

    /**
     * @return null
     */
    public function getTimeToShow()
    {
        return $this->timeToShow;
    }

    /**
     * @param null $timeToShow
     */
    public function setTimeToShow($timeToShow)
    {
        $timeToShow = (float)$timeToShow;
        if ($timeToShow) {
            $this->timeToShow = $timeToShow;
        }
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title)
    {
        $param = \Helpers\Strings::create()->clear($title);
        if ($param) {
            $this->title = $param;
        }
    }

    /**
     * @return null
     */
    public function getUrl()
    {


        return $this->url;
    }

    /**
     * @param null $url
     */
    public function setUrl($url)
    {

        $url = \Helpers\Strings::create()->clearUrl($url);
        if ($url) {
            $this->url = $url;
        }
    }

    /**
     * @return null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param null $userId
     */
    public function setUserId($userId)
    {
        $userId = \Helpers\Strings::create()->clearId($userId);
        if ($userId) {
            $this->userId = $userId;
        }
    }

    /**
     * @return null
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param null $isActive
     */
    public function setIsActive($isActive)
    {
        $isActive = \Helpers\Strings::create()->clearDigits($isActive);
        if (in_array($isActive, \Consts\OnOff::$allParams)) {
            $this->isActive = $isActive;
        }
    }


}