<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 09/10/14
 * Time: 05:46 PM
 */

namespace Entities;

use \Zend\Json\Json;

abstract class AbstractEntity
{


    abstract public function fromArray($data);

    abstract public function toArray();

    abstract public function toDb();

    abstract public function fromDb($data);

    abstract public function clear();

    public function isEmpty()
    {
        return empty($this->toArray());
    }

    public function toArrayNoNull()
    {

        $items = $this->toArray();
        $result = [];
        foreach ($items as $key => $item) {
            if (is_null($item)) {
                continue;
            }

            $result[$key] = $item;
        }
        return $result;
    }


    public function fromJson($encodedJsonString)
    {
        $encodedJsonString = (string)$encodedJsonString;

        if ($encodedJsonString) {
            return Json::decode($encodedJsonString, Json::TYPE_ARRAY);
        }

        return [];
    }


    public function toJson($data)
    {

        if (is_array($data) || is_object($data)) {
            return Json::encode($data);
        }

        return Json::encode([]);
    }
}