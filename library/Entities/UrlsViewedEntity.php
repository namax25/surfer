<?php
namespace Entities;

class UrlsViewedEntity extends \Entities\AbstractEntity
{
    protected $id = null;
    protected $dateCreated = null;
    protected $dateReport = null;
    protected $userId = null;
    protected $urlId = null;
    protected $hash = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['date_created'])) {
            $this->setDateCreated($data['date_created']);
        }
        if (isset($data['date_report'])) {
            $this->setDateReport($data['date_report']);
        }
        if (isset($data['user_id'])) {
            $this->setUserId($data['user_id']);
        }
        if (isset($data['url_id'])) {
            $this->setUrlId($data['url_id']);
        }
        if (isset($data['hash'])) {
            $this->setHash($data['hash']);
        }
    }

    public function generateHash()
    {

        if (is_null($this->getDateCreated())) {
            throw new \Exception(__METHOD__ . " Can't create a hash wihtout creation date");
        }

        if (is_null($this->getUserId())) {
            throw new \Exception(__METHOD__ . " Can't create a hash wihtout user id");
        }

        if (is_null($this->getUrlId())) {
            throw new \Exception(__METHOD__ . " Can't create a hash wihtout url id");
        }


        $this->setHash(md5(
            $this->getUserId() . $this->getDateCreated() . $this->getUrlId()
        ));
        return $this->getHash();
    }


    public function clear()
    {
        $this->id = null;
        $this->dateCreated = null;
        $this->dateReport = null;
        $this->userId = null;
        $this->urlId = null;
        $this->hash = null;
        return $this;
    }

    public function toArray()
    {
        $data = [];
        $data['id'] = $this->getId();
        $data['date_created'] = $this->getDateCreated();
        $data['date_report'] = $this->getDateReport();
        $data['user_id'] = $this->getUserId();
        $data['url_id'] = $this->getUrlId();
        $data['hash'] = $this->getHash();
        return $data;
    }

    public function toDb()
    {
        return $this->toArrayNoNull();
    }

    public function fromDb($data)
    {
        return $this->fromArray($data);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $id = \Helpers\Strings::create()->clearId($id);
        if ($id) {
            $this->id = $id;
        }
        return $this;
    }

    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function setDateCreated($dateCreated)
    {
        $dateCreated = \Helpers\DateTime::create()->check($dateCreated);
        if ($dateCreated) {
            $this->dateCreated = $dateCreated;
        }

        return $this;
    }

    public function getDateReport()
    {
        return $this->dateReport;
    }

    public function setDateReport($dateReport)
    {

        $dateReport = \Helpers\DateTime::create()->check($dateReport);
        if ($dateReport) {
            $this->dateReport = $dateReport;
        }
        return $this;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $userId = \Helpers\Strings::create()->clearId($userId);
        if ($userId) {
            $this->userId = $userId;
        }
        return $this;
    }

    public function getUrlId()
    {
        return $this->urlId;
    }

    public function setUrlId($urlId)
    {

        $urlId = \Helpers\Strings::create()->clearId($urlId);
        if ($urlId) {
            $this->urlId = $urlId;
        }

        return $this;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $hash = \Helpers\Strings::create()->clearHash($hash);
        if ($hash) {
            $this->hash = $hash;
        }
        return $this;
    }

}
