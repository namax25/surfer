<?php

namespace Entities;

use \Zend\Json\Json;

class Tariffs extends \Entities\AbstractEntity
{
    protected $id = null;
    protected $name = null;
    protected $priceMonthly = null;
    protected $params = null;
    protected $description = null;
    protected $sitesAmount = null;
    protected $referalBonuses = null;
    protected $referalPercents = null;

    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['name'])) {
            $this->setName($data['name']);
        }
        if (isset($data['price_monthly'])) {
            $this->setPriceMonthly($data['price_monthly']);
        }
        if (isset($data['params'])) {
            $this->setParams($data['params']);
        }
        if (isset($data['description'])) {
            $this->setDescription($data['description']);
        }
        if (isset($data['sites_amount'])) {
            $this->setSitesAmount($data['sites_amount']);
        }
        if (isset($data['referal_bonuses'])) {
            $this->setReferalBonuses($data['referal_bonuses']);
        }
        if (isset($data['referal_percents'])) {
            $this->setReferalPercents($data['referal_percents']);
        }
    }

    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->priceMonthly = null;
        $this->params = null;
        $this->description = null;
        $this->sitesAmount = null;
        $this->referalBonuses = null;
        $this->referalPercents = null;
        return $this;
    }

    public function toArray()
    {
        $data = [];
        $data['id'] = $this->getId();
        $data['name'] = $this->getName();
        $data['price_monthly'] = $this->getPriceMonthly();
        $data['params'] = $this->getParams();
        $data['description'] = $this->getDescription();
        $data['sites_amount'] = $this->getSitesAmount();
        $data['referal_bonuses'] = $this->getReferalBonuses();
        $data['referal_percents'] = $this->getReferalPercents();
        return $data;
    }

    public function toDb()
    {
        $data = $this->toArrayNoNull();

        if (isset($data['referal_bonuses'])) {
            $data['referal_bonuses'] = $this->toJson($data['referal_bonuses']);
        }
        if (isset($data['referal_percents'])) {
            $data['referal_percents'] = $this->toJson($data['referal_percents']);
        }
        if (isset($data['params'])) {
            $data['params'] = $this->toJson($data['params']);
        }
        return $data;
    }

    public function fromDb($data)
    {

        if (isset($data['referal_bonuses'])) {
            $data['referal_bonuses'] = $this->fromJson($data['referal_bonuses']);
        }
        if (isset($data['referal_percents'])) {
            $data['referal_percents'] = $this->fromJson($data['referal_percents']);
        }
        if (isset($data['params'])) {
            $data['params'] = $this->fromJson($data['params']);
        }
        $this->fromArray($data);
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getPriceMonthly()
    {
        return $this->priceMonthly;
    }

    public function setPriceMonthly($priceMonthly)
    {
        $this->priceMonthly = $priceMonthly;
        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getSitesAmount()
    {
        return $this->sitesAmount;
    }

    public function setSitesAmount($sitesAmount)
    {
        $this->sitesAmount = $sitesAmount;
        return $this;
    }

    public function getReferalBonuses()
    {
        return $this->referalBonuses;
    }

    public function setReferalBonuses($referalBonuses)
    {
        $this->referalBonuses = $referalBonuses;
        return $this;
    }

    public function getReferalPercents()
    {
        return $this->referalPercents;
    }

    public function setReferalPercents($referalPercents)
    {
        $this->referalPercents = $referalPercents;
        return $this;
    }


}
