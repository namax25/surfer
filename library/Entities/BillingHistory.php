<?php
/**
 * Date: 11/21/14
 * Time: 06:34 PM
 * @author namax
 */

namespace Entities;

/**
 *
 * Id
 * UserId
 * BillingTypeId
 * Date
 * Description
 * Debit
 * Credit
 * Balance
 *
 * id
 * user_id
 * billing_type_id
 * date
 * description
 * debit
 * credit
 * balance
 */
class BillingHistory extends AbstractEntity
{

    protected $id = null;
    protected $userId = null;
    protected $billingTypeId = null;
    protected $date = null;
    protected $description = null;
    protected $debit = null;
    protected $credit = null;
    protected $balance = null;


    public function fromArray($data)
    {
        if (isset($data['id'])) {
            $this->setId($data['id']);
        }
        if (isset($data['user_id'])) {
            $this->setUserId($data['user_id']);
        }
        if (isset($data['billing_type_id'])) {
            $this->setBillingTypeId($data['billing_type_id']);
        }
        if (isset($data['date'])) {
            $this->setDate($data['date']);
        }
        if (isset($data['description'])) {
            $this->setDescription($data['description']);
        }
        if (isset($data['debit'])) {
            $this->setDebit($data['debit']);
        }
        if (isset($data['credit'])) {
            $this->setCredit($data['credit']);
        }
        if (isset($data['balance'])) {
            $this->setBalance($data['balance']);
        }
    }

    public function toArray()
    {

        $data = [];

        $data['id'] = $this->getId();
        $data['user_id'] = $this->getUserId();
        $data['billing_type_id'] = $this->getBillingTypeId();
        $data['date'] = $this->getDate();
        $data['description'] = $this->getDescription();
        $data['debit'] = $this->getDebit();
        $data['credit'] = $this->getCredit();
        $data['balance'] = $this->getBalance();

        return $data;
    }

    public function toDb()
    {
        return $this->toArrayNoNull();
    }

    public function fromDb($data)
    {
        $this->fromArray($data);
        return $this;
    }

    public function clear()
    {
        $this->id = null;
        $this->userId = null;
        $this->billingTypeId = null;
        $this->date = null;
        $this->description = null;
        $this->debit = null;
        $this->credit = null;
        $this->balance = null;

        return $this;
    }

    /**
     * @return null
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param null $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    /**
     * @return null
     */
    public function getBillingTypeId()
    {
        return $this->billingTypeId;
    }

    /**
     * @param null $billingTypeId
     */
    public function setBillingTypeId($billingTypeId)
    {
        $this->billingTypeId = $billingTypeId;
        return $this;
    }

    /**
     * @return null
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param null $credit
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;
        return $this;
    }

    /**
     * @return null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param null $date
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return null
     */
    public function getDebit()
    {
        return $this->debit;
    }

    /**
     * @param null $debit
     */
    public function setDebit($debit)
    {
        $this->debit = $debit;
        return $this;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param null $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }


}