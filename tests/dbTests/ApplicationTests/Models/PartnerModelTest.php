<?php
/**
 * Date: 11/27/14
 * Time: 04:35 PM
 * @author namax
 */

namespace ApplicationTests\Models;


class PartnerModelTest extends \DbTests\AbstractDatabaseTestCase
{


    protected function getDataSet()
    {
        return new \DbTests\ArrayDataSet(array(
            'billing_history' => [],
            'users' => [
                array( // row #0
                    'id' => 1,
                    'status' => 0,
                    'login_attempts' => 0,
                    'password' => '$2y$11$9jzaKfDYBm1yA3DxAgon/ey8hku3RbOv59U6lkGTOyDeCfmhQ.yHa',
                    'email' => 'namax25@gmail.com',
                    'login' => 'namax25@gmail.com',
                    'name' => 'Maxim',
                    'balance' => 10.00,
                    'tariff' => 1,
                    'role' => 'user',
                    'permissions' => null,
                    'register_date' => '2014-11-11 20:16:54',
                    'last_activity' => '2014-11-21 21:17:00',
                    'online_mark' => null,
                    'timezone' => null,
                ),
            ]
        ));
    }

    public function testGetRowCount()
    {
        $this->assertEquals(6, $this->getConnection()->getRowCount('urls'));
        $this->assertEquals(0, $this->getConnection()->getRowCount('billing_history'));

        $daoFactory = new \Dao\DaoFactory();
        $daoFactory->setDb($this->getDb());
        $model = new \Application\Models\PartnerModel($daoFactory);

        $model->payToUserForViewedUrl(1, 1, 1);
    }

} 