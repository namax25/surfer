<?php

namespace DbTests;

/**
 * Date: 11/27/14
 * Time: 05:15 PM
 * @author namax
 */
abstract class AbstractDatabaseTestCase extends \PHPUnit_Extensions_Database_TestCase
{
    // only instantiate pdo once for test clean-up/fixture load
    static private $pdo = null;

    // only instantiate PHPUnit_Extensions_Database_DB_IDatabaseConnection once per test
    private $conn = null;

    static protected $zendDbAdapter = null;

    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO(
                    $GLOBALS['DB_DSN'],
                    $GLOBALS['DB_USER'],
                    $GLOBALS['DB_PASSWD'],
                    array(\PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'')
                );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS['DB_DBNAME']);
        }

        return $this->conn;
    }

    public function getDb()
    {
        if (is_null(self::$zendDbAdapter)) {
            $configArray = [
                'driver' => 'Pdo_MySQL',
                'username' => $GLOBALS['DB_USER'],
                'password' => $GLOBALS['DB_PASSWD'],
                'database' => $GLOBALS['DB_DBNAME'],
                'charset' => 'utf8'
            ];
            self::$zendDbAdapter = new \Zend\Db\Adapter\Adapter($configArray);
        }
        return self::$zendDbAdapter;
    }


}