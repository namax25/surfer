<?php
/**
 * User: namax
 * Date: 09/10/14
 * Time: 06:09 PM
 */


namespace EntitiesTest;


class UrlsTest extends \PHPUnit_Framework_TestCase
{


    public function testCreate()
    {
        $entity = new \Entities\Urls();


        $data['url'] = "http://google.com";
        $data['user_id'] = "2";
        $data['title'] = "goo";
        $data['time_to_show'] = "5";
        $data['shows_per_hour'] = "4";
        $data['status'] = 1;
        $data['referers'] = "http://google.com\r\nhttp://yahoo.com\r\nhttp://google.ru";
        $entity->fromArray($data);

        $data['referers'] = json_encode(explode("\r\n", $data['referers']));
        $this->assertEquals($data, $entity->toDb());
        $this->assertEquals("http://google.com", $entity->getUrl());

    }


}