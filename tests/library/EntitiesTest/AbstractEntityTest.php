<?php
/**
 * User: namax
 * Date: 09/10/14
 * Time: 06:09 PM
 */


namespace EntitiesTest;


class AbstractEntityTest extends \PHPUnit_Framework_TestCase
{


    public function testCreate()
    {
        $entity = new \EntitiesTest\EntityTest();

        $entity->setName("hello");

        $this->assertEquals(['id' => null, 'name' => 'hello'], $entity->toArray());
        $this->assertEquals(['name' => 'hello'], $entity->toArrayNoNull());

    }


}