<?php
/**
 * Created by PhpStorm.
 * User: namax
 * Date: 09/10/14
 * Time: 06:11 PM
 */

namespace EntitiesTest;


class EntityTest extends \Entities\AbstractEntity
{

    protected $id = null;
    protected $name = null;

    public function clear()
    {
        // TODO: Implement clear() method.
    }


    public function toDb()
    {
        // TODO: Implement toDb() method.
    }

    public function fromDb($data)
    {
        // TODO: Implement fromDb() method.
    }

    public function fromArray($data)
    {
    }

    public function toArray()
    {
        $data['id'] = $this->getId();
        $data['name'] = $this->getName();
        return $data;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }


}