<?php

namespace CollectionsTest;

/**
 * Description of MockObject
 *
 * @author namax
 */
class ObjectMockTest implements \Collections\ObjectInterface
{

    private $val1 = 0;

    public function setVal1($val1)
    {
        $this->val1 = $val1;
    }

    public function hashCode()
    {
        return md5($this->val1);
    }

    public function toArray()
    {
        return [$this->val1];
    }

    public function fromArray(array $data)
    {

    }


}
