<?php

namespace MailTests\GatewayProviders;

/**
 * Description of AmazonSes
 *
 * @author namax
 */
class AmazonSesTest extends \PHPUnit_Framework_TestCase
{

    /**
     *
     * @var \Mail\GatewayProviders\AmazonSes
     */
    protected $testObj = null;

    public function setUp()
    {
        $this->testObj = new \Mail\GatewayProviders\AmazonSes([]);
    }

    public function testCreate()
    {
        $this->assertInstanceOf('\Mail\GatewayProviders\AmazonSes', $this->testObj);
    }

    public function testGenerateAppropriateArgsText()
    {
        $mailParams = new \Mail\MailParams();
        $mailParams
            ->setFromAddress("reminder@citoopus.com")
            ->setToAddresses(['test1@gmail.com', 'test2@gmail.com'])
            ->setCcAddresses(['test3@gmail.com', 'test4@gmail.com'])
            ->setBccAddresses(['test5@gmail.com', 'test6@gmail.com'])
            ->setMessageSubject("так просто")
            ->setBodyText("другие данные")
            ->setReplyToAddresses(['emergency@citoopus.com'])
            ->setReturnPath('emergency@citoopus.com');


        $expected = array(
            'Source' => 'reminder@citoopus.com',
            'Destination' => array(
                'ToAddresses' => array('test1@gmail.com', 'test2@gmail.com'),
                'CcAddresses' => array('test3@gmail.com', 'test4@gmail.com'),
                'BccAddresses' => array('test5@gmail.com', 'test6@gmail.com'),
            ),
            'Message' => array(
                'Subject' => array(
                    'Data' => 'так просто',
                    'Charset' => 'utf8',
                ),
                'Body' => array(
                    'Text' => array(
                        'Data' => 'другие данные',
                        'Charset' => 'utf8',
                    )
                ),
            ),
            'ReplyToAddresses' => array('emergency@citoopus.com'),
            'ReturnPath' => 'emergency@citoopus.com',
        );

        $this->assertEquals($expected, $this->testObj->generateAppropriateArgs($mailParams));
    }

    public function testGenerateAppropriateArgsHtml()
    {
        $mailParams = new \Mail\MailParams();
        $mailParams
            ->setFromAddress("reminder@citoopus.com")
            ->setToAddresses(['test1@gmail.com', 'test2@gmail.com'])
            ->setCcAddresses(['test3@gmail.com', 'test4@gmail.com'])
            ->setBccAddresses(['test5@gmail.com', 'test6@gmail.com'])
            ->setMessageSubject("так просто")
            ->setBodyText("другие данные")
            ->setBodyHtml("<h1>hello</h1>другие данные")
            ->setReplyToAddresses(['emergency@citoopus.com'])
            ->setReturnPath('emergency@citoopus.com');


        $expected = array(
            'Source' => 'reminder@citoopus.com',
            'Destination' => array(
                'ToAddresses' => array('test1@gmail.com', 'test2@gmail.com'),
                'CcAddresses' => array('test3@gmail.com', 'test4@gmail.com'),
                'BccAddresses' => array('test5@gmail.com', 'test6@gmail.com'),
            ),
            'Message' => array(
                'Subject' => array(
                    'Data' => 'так просто',
                    'Charset' => 'utf8',
                ),
                'Body' => array(
                    'Text' => array(
                        'Data' => 'другие данные',
                        'Charset' => 'utf8',
                    ),
                    'Html' => array(
                        'Data' => '<h1>hello</h1>другие данные',
                        'Charset' => 'utf8',
                    )
                ),
            ),
            'ReplyToAddresses' => array('emergency@citoopus.com'),
            'ReturnPath' => 'emergency@citoopus.com',
        );

        $this->assertEquals($expected, $this->testObj->generateAppropriateArgs($mailParams));
    }
}
