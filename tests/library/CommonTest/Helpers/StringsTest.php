<?php

namespace CommonTest\Helpers;

use  \Helpers\Strings;

/**
 * Description of Ads
 * Помощник для работы с объявлениями
 * @author namax
 */
class StringsTest extends \PHPUnit_Framework_TestCase
{

    public function testCreate()
    {
        $this->assertInstanceOf('\Helpers\Strings', Strings::create());
    }

    public function providerStrings()
    {
        return array(
            array("Проверка содержит ли строка заданную подстроку", "ПроВерка", true, true, true),
            array("Проверка содержит ли строка заданную подстроку", "проверка", true, true, true),
            array("Проверка содержит ли строка заданную подстроку", "Проверка", true, false, true),
            array("Проверка содержит ли строка заданную подстроку", "проверка", true, false, false),
            array("class StringsTests", "StringsTests", false, false, true),
            array("class StringsTests", "stringsTests", false, false, false),
            array("class StringsTests", "stringstests", false, true, true),
            array("class StringsTests", "stringsTests", false, true, true),
        );
    }

    /**
     * @dataProvider providerStrings
     */
    public function testIsStrContain($string, $substring, $utf, $caseInsensitive, $expected)
    {
        $this->assertEquals($expected, Strings::create()->isStrContain($substring, $string, $utf, $caseInsensitive));
    }

    public function providerUcfirst()
    {
        return array(
            array("Проверка", true, "Проверка"),
            array("stringsTests", false, "StringsTests"),
            array("StringsTests", false, "StringsTests"),
            array("проверка", true, "Проверка"),
        );
    }

    /**
     * @dataProvider providerUcfirst
     */
    public function testUcfirst($str, $utf, $expected)
    {
        $this->assertEquals($expected, Strings::create()->ucfirst($str, $utf));
    }

    public function providerBadArrStrings()
    {
        return array(
            array("<sd\'xcv!", "sdxcv!"),
            array(["<sd\'xcv!", ">sd\'xcv#!"], ['sdxcv!', 'sdxcv#!']),
        );
    }

    /**
     * @dataProvider providerBadArrStrings
     */
    public function testClear($str, $expected)
    {
        $this->assertEquals($expected, Strings::create()->clear($str));
    }

    public function providerBadArrDigitsStrings()
    {
        return array(
            array("<s1d\'xc4v!", "14"),
            array(["<s3d\'xc4v!", ">5sd\'x1cv#!"], ['34', '51']),
        );
    }

    /**
     * @dataProvider providerBadArrDigitsStrings
     */
    public function testClearDigits($str, $expected)
    {
        $this->assertEquals($expected, Strings::create()->clearDigits($str));
    }

    public function providerAliasStrings()
    {
        return array(
            array("<s1d\'xc4v!_fd", "s1dxc4v_fd"),
            array(["<s3d\'xc4v!_fd23", ">5sd\'x1cv#!_dsf3"], ['s3dxc4v_fd23', '5sdx1cv_dsf3']),
        );
    }

    /**
     * @dataProvider providerAliasStrings
     */
    public function testClearAlias($str, $expected)
    {

        $this->assertEquals($expected, Strings::create()->clearAlias($str));
    }


    public function providerUrlStrings()
    {
        return array(
            ["http://foo.com/blah_blah", "http://foo.com/blah_blah"],
            ["http://foo.com/blah_blah/", "http://foo.com/blah_blah"],
            ["http://foo.com/blah_blah_(wikipedia)", "http://foo.com/blah_blah_(wikipedia"],
            ["http://foo.com/blah_blah_(wikipedia)_(again)", "http://foo.com/blah_blah_(wikipedia)_(again"],
            ["http://www.example.com/wpstyle/?p=364", "http://www.example.com/wpstyle/?p=364"],
            ["https://www.example.com/foo/?bar=baz&inga=42&quux", "https://www.example.com/foo/?bar=baz&inga=42&quux"],
            ["http://✪df.ws/123", false],
            ["http://userid:password@example.com:8080", "http://userid:password@example.com:8080"],
            ["http://userid:password@example.com:8080/", "http://userid:password@example.com:8080"],
            ["http://userid@example.com", "http://userid@example.com"],
            ["http://userid@example.com/", "http://userid@example.com"],
            ["http://userid@example.com:8080", "http://userid@example.com:8080"],
            ["http://userid@example.com:8080/", "http://userid@example.com:8080"],
            ["http://userid:password@example.com", "http://userid:password@example.com"],
            ["http://userid:password@example.com/", "http://userid:password@example.com"],
            ["http://142.42.1.1/", "http://142.42.1.1"],
            ["http://142.42.1.1:8080/", "http://142.42.1.1:8080"],
            ["http://➡.ws/䨹", false],
            ["http://⌘.ws", false],
            ["http://⌘.ws/", false],
            ["http://foo.com/blah_(wikipedia)#cite-1", "http://foo.com/blah_(wikipedia)#cite-1"],
            ["http://foo.com/blah_(wikipedia)_blah#cite-1", "http://foo.com/blah_(wikipedia)_blah#cite-1"],
            ["http://foo.com/unicode_(✪)_in_parens", false],
            ["http://foo.com/(something)?after=parens", "http://foo.com/(something)?after=parens"],
            ["http://☺.damowmow.com/", false],
            ["http://code.google.com/events/#&product=browser", "http://code.google.com/events/#&product=browser"],
            ["http://j.mp", "http://j.mp"],
            ["ftp://foo.bar/baz", false],
            ["http://foo.bar/?q=Test%20URL-encoded%20stuff", "http://foo.bar/?q=Test%20URL-encoded%20stuff"],
            ["http://مثال.إختبار", false],
            ["http://例子.测试", false],
            ["http://उदाहरण.परीक्षा", false],
            ["http://-.~_!$&'()*+,;=:%40:80%2f::::::@example.com", false],
            ["http://1337.net", "http://1337.net"],
            ["http://a.b-c.de", "http://a.b-c.de"],
            ["http://223.255.255.254", "http://223.255.255.254"],
            ["http://", false],
            ["http://.", false],
            ["http://..", false],
            ["http://../", false],
            ["http://?", false],
            ["http://??", false],
            ["http://??/", false],
            ["http://#", false],
            ["http://##", false],
            ["http://##/", false],
            ["http://foo.bar?q=Spaces should be encoded", false],
            ["rdar://1234", false],
            ["http:// shouldfail.com", false],
            [":// should fail", false],
            ["http://foo.bar/foo(bar)baz quux", false],
            ["http://-error-.invalid/", "http://-error-.invalid"],
            ["http://a.b--c.de/", "http://a.b--c.de"],
            ["http://-a.b.co", "http://-a.b.co"],
            ["http://a.b-.co", "http://a.b-.co"],
            ["http://0.0.0.0", "http://0.0.0.0"],
            ["http://10.1.1.0", "http://10.1.1.0"],
            ["http://10.1.1.255", "http://10.1.1.255"],
            ["http://224.1.1.1", "http://224.1.1.1"],
            ["http://1.1.1.1.1", "http://1.1.1.1.1"],
            ["http://123.123.123", "http://123.123.123"],
            ["http://3628126748", "http://3628126748"],
            ["http://.www.foo.bar/", "http://.www.foo.bar"],
            ["http://www.foo.bar./", "http://www.foo.bar"],
            ["http://.www.foo.bar./", "http://.www.foo.bar"],
            ["http://10.1.1.1", "http://10.1.1.1"],
            ["http://google.com", "http://google.com"],
        );
    }

    /**
     *
     * @dataProvider providerUrlStrings
     *
     */
    public function testClearUrl($str, $expected)
    {
        $this->assertEquals($expected, Strings::create()->clearUrl($str));
    }


    public function providerFloats()
    {
        return array(
            [10.2, 10.2],
            ['10.2', 10.2],
            ["10.2http://foocom/blah_blah/", 10.2],
            [5, 5],
            [null, ''],
        );
    }

    /**
     *
     * @dataProvider providerFloats
     *
     */
    public function testClearFloat($str, $expected)
    {
        $this->assertEquals($expected, Strings::create()->clearFloat($str));
    }

}
