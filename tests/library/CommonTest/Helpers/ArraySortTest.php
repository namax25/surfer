<?php

namespace CommonTest\Helpers;

use  \Helpers\ArraySort;

/**
 * Description of Ads
 * Помощник для работы с объявлениями
 * @author namax
 */
class ArraySortTest extends \PHPUnit_Framework_TestCase
{

    public function testCreate()
    {
        $this->assertInstanceOf('\Helpers\ArraySort', ArraySort::create());
    }

    public function providerStrings()
    {
        return array(
            array(
                [['id' => 11, "name" => "max"], ['id' => 22, "name" => "john"]],
                'id',
                [11 => ['id' => 11, "name" => "max"], 22 => ['id' => 22, "name" => "john"]]
            ),
        );
    }

    /**
     * @dataProvider providerStrings
     */
    public function testSetColumnAsKey($array, $column, $expected)
    {
        $this->assertEquals($expected, ArraySort::create()->setColumnAsKey($array, $column));
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Helpers\ArraySort::setColumnAsKey Expects parameter 1 to be array, integer given
     */
    public function testSetColumnAsKeyException()
    {
        ArraySort::create()->setColumnAsKey([1, 2], "id");
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Helpers\ArraySort::setColumnAsKey There is no column with name id
     */
    public function testSetColumnAsKeyExceptionForUnknownColumn()
    {
        ArraySort::create()->setColumnAsKey([[1], [2]], "id");
    }
}
