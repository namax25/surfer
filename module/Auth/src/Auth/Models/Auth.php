<?php

namespace Auth\Models;

/**
 * Description of Auth
 *
 * @author namax
 */
class Auth
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function getByLogin($login)
    {
        return $this->getDaoUsers()->getByLogin($login);
    }

    public function updateLoginAttempts($login, $loginAttempts)
    {
        $loginAttempts = (int)(string)$loginAttempts;
        if ($loginAttempts <= 0) {
            $loginAttempts = 0;
        }

        return $this->getDaoUsers()->update(['login_attempts' => $loginAttempts], ['login' => $login]);
    }

    /**
     *
     * @return \Dao\Users
     */
    public function getDaoUsers()
    {
        return $this->getDaoFactory()->getDaoUsers();
    }
}
