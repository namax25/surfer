<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Forms;
use Application\InputFilters;

class SiteController extends AbstractActionController
{

    private $authService = null;


    protected $formAddSite;
    protected $messages = null;


    /**
     * Модель для работы с сайтами
     * @var \Application\Models\SiteModel
     */
    protected $modelSite = null;

    const ITEMS_PER_PAGE = 20;

    public function getAuthService()
    {
        if (!$this->authService) {
            $this->authService = $this->getServiceLocator()->get('AuthenticationService');
        }
        return $this->authService;
    }


    /**
     *
     * @return \Application\Forms\AddSiteForm()
     */
    public function getAddSiteForm()
    {
        if (!$this->formAddSite) {
            $form = new \Application\Forms\AddSiteForm();
            $form->prepareElements();
            $filters = new \Application\InputFilters\AddSiteFormFilters();
            $form->setInputFilter($filters->getInputFilter());
            $this->formAddSite = $form;
        }

        return $this->formAddSite;
    }

    /**
     *
     * @return \App\Controllers\Messenger
     */
    public function getMessages()
    {
        if ($this->messages === null) {
            $this->messages = new \App\Controllers\Messenger();
        }

        return $this->messages;
    }

    /**
     *
     * @return \Application\Models\SiteModel
     */
    public function getModelSite()
    {
        if ($this->modelSite === null) {
            $this->modelSite = new \Application\Models\SiteModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelSite;
    }


    public function addAction()
    {

        $request = $this->getRequest();
        $this->getMessages()->clearAllMessages();

        $userData = $this->getAuthService()->getIdentity();

        if ($request->isPost()) {

            $this->getAddSiteForm()->setData($request->getPost());
            $this->getAddSiteForm()->isValid();

            if ($this->getAddSiteForm()->isValid()) {
                $entityUrls = new \Entities\Urls();
                $data = $this->getAddSiteForm()->getData();
                $entityUrls->fromArray($data);
                $entityUrls->setUserId($userData->id);
                $this->getModelSite()->insert($entityUrls);

                $this->getMessages()->addSuccessMessage("Успешно обновлено");
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }

        $viewModel = new ViewModel([
            'form' => $this->getAddSiteForm(),
            'messages' => $this->getMessages()
        ]);

        return $viewModel;
    }


    public function listAction()
    {


        $page = 0;
        $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();

        $list = $this->getModelSite()->getList($idenity->id);
        $list->setCurrentPageNumber((int)$this->params()->fromQuery('page', 1));
        $list->setItemCountPerPage(self::ITEMS_PER_PAGE);
        $viewModel = new ViewModel();
        $viewModel->setVariable('itemsList', $list);
        return $viewModel;
    }

    public function editAction()
    {
        $this->getMessages()->clearAllMessages();

        $id = (int)$this->params()->fromRoute('id', 0);

        $request = $this->getRequest();

        if (!$id) {
            return $this->redirect()->toRoute('app/default', array(
                'controller' => 'site',
                'action' => 'add'
            ));
        }
        $userData = $this->getAuthService()->getIdentity();

        $entityUrls = new \Entities\Urls();

        if ($request->isPost()) {
            $this->getAddSiteForm()->setData($request->getPost());

            if ($this->getAddSiteForm()->isValid()) {

                $entityUrls->fromArray($this->getAddSiteForm()->getData());
                $entityUrls->setUserId($userData->id);

                $this->getModelSite()->update($entityUrls, $entityUrls->getId());

                $this->getMessages()->addSuccessMessage("Успешно обновлено");
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }

        $data = $this->getModelSite()->getSite($userData->id, $id);

        if (empty($data)) {
            return $this->redirect()->toRoute('app/default', array(
                'controller' => 'site',
                'action' => 'add'
            ));
        }

        $data = $entityUrls->clear()->fromDb($data)->toArray();
        $data['referers'] = implode("\r\n", $data['referers']);
        $this->getAddSiteForm()->setData($data);


        $viewModel = new ViewModel(
            ['form' => $this->getAddSiteForm(), 'id' => $id, 'messages' => $this->getMessages()]
        );

        return $viewModel;
    }

    public function deleteAction()
    {
        $id = (int)$this->params()->fromRoute('id', 0);

        $userData = $this->getAuthService()->getIdentity();


        if ($id && $userData->id) {
            $this->getModelSite()->deleteSite($userData->id, $id);
        }
        return $this->redirect()->toRoute('app/default', array(
            'controller' => 'site',
            'action' => 'list'
        ));
    }
}
