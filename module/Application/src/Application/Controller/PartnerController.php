<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Forms;
use Application\InputFilters;
use App\Controllers\Responses\JsonStringModel;
use Datatypes\Redis;
use Zend\Json\Json;
use \App\Controllers\Responses\JsonResponse;

class PartnerController extends AbstractController
{


    protected $listUrlsForSendToClient = null;


    /**
     *
     * @var \Application\Models\DashboardModel
     */
    protected $modelDashboard = null;

    /**
     * @var \Application\Models\PartnerModel
     */
    protected $modelPartner = null;

    public function indexAction()
    {
        $userData = $this->getAuthService()->getIdentity();

//        var_dump($this->getModelPartner()->getDataForIndexTable($userData));
       //var_dump($this->getDaoFactory()->getTariff()->getById($userData->tariff));
        $tariffData =$this->getDaoFactory()->getTariff()->getById($userData->tariff);

        $viewModel = new ViewModel(
            [
                'userData' => $this->getAuthService()->getStorage()->read(),
                'host' => $this->getConfig('site')['host'],
                'tariffData' => $tariffData

            ]
        );
        return $viewModel;
    }

    public function urlsAction()
    {

        try {

            $jsonResponse = new \App\Controllers\Responses\JsonResponse();


            if (!$this->getAuthService()->hasIdentity()) {
                $jsonResponse->addErr("Вы не авторизированы");
                return new JsonModel($jsonResponse);
            }

            $userData = $this->getAuthService()->getIdentity();

//           $this->getResponse()->setStatusCode(403);
            $key = 'user:' . $userData->id;

            if ($this->getRedis()->exists($key)) {
                $jsonResponse->addErr("Нельзя получить следующую ссылку, не просмотрев уже существующую");
                return new JsonModel($jsonResponse);
            }

            $urlDataString = $this->getListUrlsForSendToClient()->rPop();

            if (empty($urlDataString)) {
                $jsonResponse->addInfo("В очереди нет ссылок. Подождите пока появятся");
                return $jsonResponse;
            }


            $urlData = Json::decode($urlDataString);

            $lastIdInViewedUrls = $this->getModelPartner()->saveUrlReturnedToView($userData->id, $urlData->id);

            if ($lastIdInViewedUrls === false) {
                $jsonResponse->addInfo("Не удалось сгенерировать ссылку. Повторите попытку");
                return $jsonResponse;
            }

            $timeout = (int)ceil($urlData->time_to_show) * \Consts\Time::MIN_IN_SEC;
            $redisKey = new Redis\Key($this->getRedis());
            $redisKey->expire($key, $userData->id, $timeout);

            $urlData->hash = $this->getModelPartner()->getHashOfViewedUrl($userData->id, $urlData->id);
            $urlData->lid = $lastIdInViewedUrls;
            $jsonResponse->setPayload($this->getModelPartner()->prepareUrlDataToSendToClient($urlData));

        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $jsonResponse->addErr("Ошибка получения данных");
        }

        return new JsonModel($jsonResponse);

    }

    public function reportAction()
    {
        try {
            $jsonResponse = new JsonResponse();


            if (!$this->getAuthService()->hasIdentity()) {
                $jsonResponse->addErr("Вы не авторизированы");
                return new JsonModel($jsonResponse);
            }

            $userData = $this->getAuthService()->getIdentity();

            $key = 'user:' . $userData->id;

            if ($this->getRedis()->exists($key)) {
                $jsonResponse->addErr("Время просмотра по Вашей ссылке еще не истекло");
                return new JsonModel($jsonResponse);
            }


            if ($this->getRequest()->isPost()) {

                $reportHash = \Helpers\Strings::create()->clearHash($this->params()->fromPost('hash', null));
                $linkId = $this->params()->fromPost('lid', null);


                $urlViewedByUser = $this->getDaoFactory()->getUrlsViewed()->getById($linkId);

                if ($urlViewedByUser === false || $urlViewedByUser->hash != $reportHash) {
                    $jsonResponse->addErr("Ошибка получения ссылки");
                    return new JsonModel($jsonResponse);
                }

                if ($urlViewedByUser->date_report) {
                    $jsonResponse->addInfo("Отчет о просмотре ссылки уже составлен");
                    return new JsonModel($jsonResponse);
                }


                $this->getModelPartner()->updateViewedUrl((array)$urlViewedByUser);


                $urlId = $this->params()->fromPost('url', 0);
                $lastInsertedId = $this->getModelPartner()->payToUserForViewedUrl(
                    $userData->tariff,
                    $userData->id,
                    $urlViewedByUser->url_id
                );

                if ($lastInsertedId) {
                    $jsonResponse->addSuccess("Отчет успешно принят");
                }

            } else {
                $jsonResponse->addErr("Ошибка получения данных");
            }
        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $jsonResponse->addErr("Ошибка обновления данных");
        }
        return new JsonModel($jsonResponse);

    }

    /**
     *
     * @return \Application\Models\UserModel
     */
    protected function getModelUser()
    {
        if ($this->modelDashboard === null) {
            $this->modelDashboard = new \Application\Models\UserModel($this->getDaoFactory());
        }
        return $this->modelDashboard;
    }

    /**
     *
     * @return \Application\Models\PartnerModel
     */
    protected function getModelPartner()
    {
        if ($this->modelPartner === null) {
            $this->modelPartner = new \Application\Models\PartnerModel($this->getDaoFactory());
        }
        return $this->modelPartner;
    }


    /**
     * @return Redis\ListUrlsForSendToClient|null
     */
    protected function getListUrlsForSendToClient()
    {

        if (is_null($this->listUrlsForSendToClient)) {
            $this->listUrlsForSendToClient = new Redis\ListUrlsForSendToClient($this->getRedis());
        }

        return $this->listUrlsForSendToClient;
    }


}
