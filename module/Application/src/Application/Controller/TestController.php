<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class TestController extends AbstractActionController
{

    private $authService = null;

    public function indexAction()
    {
//        $di = new Di();
//        $test = $di->get('Common\Helpers\Test', array('data' => 'some data'));
//        $reader = new \Zend\Config\Reader\Json();
//        $data = $reader->fromString('{"from":"SmartAgent","fromagent":"ListAgent"}');
//        $config = new \Zend\Config\Config($data);
//        var_dump($data, $config->from123);
//        $test->foo();
//        $redis = new \Redis();
//        $redis->connect('127.0.0.1');
//        $count = $redis->dbSize();
//echo "Redis has $count keys\n";
//$redis->set("hello_world", "Hi from php!");
//$value = $redis->get("hello_world");
//var_dump($value);
//$redis->publish("the_channel", "this is a test");
//        $this->sesscontainer = new \Zend\Session\Container('sess_acl2');
//        $this->sesscontainer->hi = "hello";
//        var_dump($this->sesscontainer->hi);
//        if ($this->getAuthService()->hasIdentity()) {
//            echo "cool";
////            var_dump($this->getAuthService()->getIdentity());
//        } else {
//            echo \Helpers\Strings::create()->clearDigits("sdf234sdf453");
//        }
        return new ViewModel();
    }

    public function anyAction()
    {
//        $request = $this->getRequest();
//
        $jsonResponse = new \App\Controllers\Responses\JsonResponse();
//        $userData = $this->getAuthService()->getIdentity();
//        try {
//            if ($request->isPost()) {
//                $data = [];
//                $data['post'] = $request->getPost();
//                $date = new \DateTime($this->params()->fromPost('term', 0));
//                $data['date'] = \Helpers\DateTime::create()->printDate($date);
//
//                $jsonResponse->setPayload($data);
//                $jsonResponse->addDebug($this->params()->fromPost('term', 0));
//            }
//        } catch (\Exception $e) {
//            $this->getLogDb()->err($e);
//            $jsonResponse->addErr("Ошибка получения данных");
//        }
        return new JsonModel($jsonResponse);
    }

    public function getAuthService()
    {
        if (!$this->authService) {
            $this->authService = $this->getServiceLocator()->get('AuthenticationService');
        }
        return $this->authService;
    }
}
