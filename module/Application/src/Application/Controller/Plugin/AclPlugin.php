<?php

namespace Application\Controller\Plugin;

/**
 * Description of AclPlugin
 *
 * @author namax
 */
use Zend\Mvc\Controller\Plugin\AbstractPlugin,
    Zend\Session\Container as SessionContainer,
    Zend\Permissions\Acl\Acl,
    Zend\Permissions\Acl\Role\GenericRole as Role,
    Zend\Permissions\Acl\Resource\GenericResource as Resource;

class AclPlugin extends AbstractPlugin
{

    protected $sesscontainer;

    /**
     *
     * @return \Zend\Session\Container
     */
    private function getSessContainer()
    {
        if (!$this->sesscontainer) {
            $this->sesscontainer = new SessionContainer('sess_acl');
        }
        return $this->sesscontainer;
    }

    public function doAuthorization(\Zend\Mvc\MvcEvent $e, $sm)
    {
        $acl = new Acl();
        $roles = [
            'anonymous',
            'user',
            'admin'
        ];

        $acl->addRole(new Role('anonymous'));
        $acl->addRole(new Role('user'), 'anonymous');
        $acl->addRole(new Role('admin'), 'user');

        $acl->addResource(new Resource('Application'));
        $acl->addResource(new Resource('Auth'));

        $acl->allow('anonymous', 'Auth');
        $acl->allow('anonymous', 'Application', ['index:index']);
        $acl->allow('anonymous', 'Application', [
            'user:register',
            'user:success-registration',
        ]);

        $acl->allow('anonymous', 'Application', [
            'partner:urls',
            'partner:report',
        ]);


        $acl->allow('user', 'Application', ['test:any', 'test:index', 'index:dashboard']);
        $acl->allow('user', 'Application', ['site:add', 'site:list', 'site:edit', 'site:delete']);

        $acl->allow('user', 'Application', [
            'profile:index',
            'profile:timezone',
            'profile:change-password',
        ]);

        $acl->allow('user', 'Application', [
            'partner:index',
        ]);


        //admin is child of user, can publish, edit, and view too !
        $acl->allow('admin', ['Application'], array('publish', 'edit'));
        $authService = $sm->get('AuthenticationService');
        $idenity = $authService->getIdentity();
        if ($idenity) {
            $idd = $idenity->id;
        }

        $resource = $this->getResource($e);
        $role = $this->getRole($roles);
        $privilege = strtolower($this->getPrivilege($e));
//        var_dump("-", $role, $resource, $privilege, $acl->isAllowed($role, $resource, $privilege));
//        die();
        //проверка и редирект
        if (!$acl->isAllowed($role, $resource, $privilege)) {
            $router = $e->getRouter();
            $url = $router->assemble(array(), array('name' => 'auth'));

            $response = $e->getResponse();
            $response->setStatusCode(302);
            $response->getHeaders()->addHeaderLine('Location', $url);
            $e->stopPropagation();
        }
    }

    protected function getResource($e)
    {
        $controllerClass = get_class($e->getTarget());
        return substr($controllerClass, 0, strpos($controllerClass, '\\'));
    }

    /**
     * Получаем роль из сессии или ставим по умолчанию
     */
    protected function getRole($roles)
    {
        if ($this->getSessContainer()->offsetExists('userData') &&
            in_array($this->getSessContainer()->userData->role, $roles)
        ) {
            return $this->getSessContainer()->userData->role;
        }
        return 'anonymous';
    }

    /**
     *
     * формируем привилегии из имени контроллера и метода
     * @param $e
     * @return string
     */
    protected function getPrivilege($e)
    {
        $routeMatch = $e->getRouteMatch();
        $controller = $routeMatch->getParam('__CONTROLLER__');
        $action = $routeMatch->getParam('action');
        return $controller . ":" . $action;
    }

}
