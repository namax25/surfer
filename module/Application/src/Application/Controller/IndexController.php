<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Consts\Table\Tariffs\Id;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Forms;
use Application\InputFilters;
use Zend\Http\Header\SetCookie;


class IndexController extends AbstractController
{

    const ACTIVE_DAYS_REFERRAL_LINK = 5;
    /**
     *
     * @var \Application\Models\DashboardModel
     */
    protected $modelDashboard = null;

    public function indexAction()
    {

      /*  $redis = new \Redis() or die("Can not load redis.");
        if (!$redis->connect('127.0.0.1')) {

        }

        if ($redis->get('test_key')) {

            echo " I have a key. " . $redis->get('test_key');

        } else {

            echo " Loose the key. ";
            $redis->multi();
            $redis->set('test_key', 2);
            $redis->expire('test_key', 20);
            $redis->exec();
        }

        var_dump($redis->get('test_key'));*/


        $refId = $this->params()->fromRoute('id', 0);

        if ($refId && empty($this->getRequest()->getCookie()->rfl)) {
            $cookie = new SetCookie(
                'rfl', ($refId), time() + self::ACTIVE_DAYS_REFERRAL_LINK * 60 * 60 * 24, "/"
            );
            $response = $this->getResponse()->getHeaders();
            $response->addHeader($cookie);
        }

        $viewModel = new ViewModel(['userData' => $this->getAuthService()->getStorage()->read()]);
        return $viewModel;
    }

    public function dashboardAction()
    {

        $idenity = $this->getServiceLocator()->get('AuthenticationService')->getIdentity();
        $viewModel = new ViewModel();
        return $viewModel;
    }

    /**
     *
     * @return \Application\Models\DashboardModel
     */
    public function getModelDashboard()
    {
        if ($this->modelDashboard === null) {
            $this->modelDashboard = new \Application\Models\DashboardModel($this->getServiceLocator()->get('DaoFactory'));
        }
        return $this->modelDashboard;
    }
}
