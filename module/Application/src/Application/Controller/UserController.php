<?php
/**
 * User: namax
 * Date: 28/10/14
 * Time: 03:48 PM
 */

namespace Application\Controller;

use Zend\View\Model\ViewModel;

class UserController extends AbstractController
{


    protected $formRegister;

    protected $modelUser;
    protected $modelPartner;

    public function registerAction()
    {

        $request = $this->getRequest();
        $this->getMessages()->clearAllMessages();

        if ($request->isPost()) {
            $this->getRegisterForm()->setData($request->getPost());

            if ($this->getRegisterForm()->isValid()) {

                $data = $this->getRegisterForm()->getData();

                try {
                    $newUserId = $this->getModelUser()->register($data['email'], $this->getMailSevice());

                    if ($newUserId === false) {
                        throw new \Exception(__METHOD__ . " Can't Register User");
                    }

                    $this->checkReferralCookieAndCreateReferral($newUserId);

                    return $this->redirect()->toRoute(
                        'app/default',
                        ['controller' => 'user', 'action' => 'success-registration']
                    );

                } catch (\Exception $e) {
                    $this->getLogDb()->err($e);
                    $this->getMessages()->addErrorMessage("Такой email уже зарегистрирован на сайте");
                }

            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }

        }

        $this->getRegisterForm()->setData($request->getPost());

        $viewModel = new ViewModel([
            'form' => $this->getRegisterForm(),
            'messages' => $this->getMessages()
        ]);
        return $viewModel;

    }

    protected function checkReferralCookieAndCreateReferral($newUserId)
    {
        $this->getModelPartner()->createRefferal($this->getReferralIdFromCookie(), $newUserId);
    }

    protected function getReferralIdFromCookie()
    {
        $referralId = !empty($this->getRequest()->getCookie()->rfl) ?
            $this->getRequest()->getCookie()->rfl : 0;
        return \Helpers\Strings::create()->clearDigits($referralId);
    }


    public function successRegistrationAction()
    {
        $viewModel = new ViewModel();
        return $viewModel;
    }

    /**
     *
     * @return \Application\Forms\RegisterForm
     *
     */
    public function getRegisterForm()
    {
        if (!$this->formRegister) {
            $form = new \Application\Forms\RegisterForm();
            $form->prepareElements();
            $filters = new \Application\InputFilters\RegisterFormFilters();
            $form->setInputFilter($filters->getInputFilter());
            $this->formRegister = $form;
        }

        return $this->formRegister;
    }

    /**
     *
     * @return \Application\Models\UserModel
     */
    public function getModelUser()
    {
        if ($this->modelUser === null) {
            $this->modelUser = new \Application\Models\UserModel($this->getDaoFactory());
        }
        return $this->modelUser;
    }


    public function getModelPartner()
    {

        if ($this->modelPartner === null) {
            $this->modelPartner = new \Application\Models\PartnerModel($this->getDaoFactory());
        }
        return $this->modelPartner;
    }
}