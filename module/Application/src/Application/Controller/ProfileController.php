<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class ProfileController extends AbstractController
{

    protected $modelProfile = null;
    protected $formProfile = null;
    protected $formChangePassword;


    public function indexAction()
    {

        $request = $this->getRequest();
        $this->getMessages()->clearAllMessages();

        $userData = $this->getAuthService()->getIdentity();

        if ($request->isPost()) {
            $this->getProfileForm()->setData($request->getPost());

            if ($this->getProfileForm()->isValid()) {
                $this->getModelProfile()->update($this->getProfileForm()->getData(), $userData->id);
                $this->getMessages()->addSuccessMessage("Успешно обновлено");
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }

        $data = $this->getModelProfile()->getUserProfileByLogin($userData->login);
        $this->getAuthService()->getStorage()->write($data);
        setcookie("timezone", $data->timezone, 0, "/");
        $this->getProfileForm()->setData($data);
        $viewModel = new ViewModel([
            'form' => $this->getProfileForm(),
            'id' => $userData->id,
            'messages' => $this->getMessages()
        ]);
        return $viewModel;
    }

    public function changePasswordAction()
    {
        $this->getMessages()->clearAllMessages();
        $userData = $this->getAuthService()->getIdentity();
        $request = $this->getRequest();


        if ($request->isPost()) {
            $this->getChangePasswordForm()->setData($request->getPost());

            if ($this->getChangePasswordForm()->isValid()) {

                $data = $this->getChangePasswordForm()->getData();

                $isValidLoginPass = $this->getAuthService()->getAdapter()
                    ->isValidLoginPass($userData->login, $data['password'], $userData->id);

                if ($isValidLoginPass) {
                    $hash = $this->getAuthService()->getAdapter()->generateNewHash($userData, $data['new_password']);
                    $this->getModelProfile()->updatePasswordHash($hash, $userData->id);
                    $this->getMessages()->addSuccessMessage("Успешно обновлено");
                } else {
                    $this->getMessages()->addErrorMessage("Ваш прежний пароль не верен");
                }
            } else {
                $this->getMessages()->addErrorMessage("Данные формы не корректны");
            }
        }


        $viewModel = new ViewModel([
            'form' => $this->getChangePasswordForm(),
            'id' => $userData->id,
            'messages' => $this->getMessages()
        ]);
        return $viewModel;
    }

    /**
     * Для определения часового пояса  пользователя, если не установлен
     * @return \Zend\View\Model\JsonModel
     */
    public function timezoneAction()
    {

        try {
            $jsonResponse = new \App\Controllers\Responses\JsonResponse();
            $timezone = $this->params()->fromQuery('timezone', 0);
            $userData = $this->getAuthService()->getIdentity();

            if (!$userData->timezone) {
                $jsonResponse->addInfo("Установлен часовой пояс " . $timezone);
                $timezone = preg_replace("/[^0-9a-z\_\/]+/iu", "", $timezone);
                $this->getModelProfile()->update(['timezone' => $timezone], $userData->id);
                $data = $this->getModelProfile()->getUserProfileByLogin($userData->login);
                $this->getAuthService()->getStorage()->write($data);
                setcookie("timezone", $timezone, 0, "/");

                $jsonResponse->setPayload(['timezone' => $timezone]);
            } else {
                setcookie("timezone", $userData->timezone, 0, "/");

                $jsonResponse->setPayload(['timezone' => $userData->timezone]);
            }
        } catch (\Exception $e) {
            $this->getLogDb()->err($e);
            $jsonResponse->addErr("Ошибка получения данных");
        }
        return new JsonModel($jsonResponse);
    }

    /**
     *
     * @return \Application\Forms\ProfileForm()
     */
    public function getProfileForm()
    {
        if (!$this->formProfile) {
            $form = new \Application\Forms\ProfileForm();
            $form->prepareElements();
            $filters = new \Application\InputFilters\ProfileFormFilters();
            $form->setInputFilter($filters->getInputFilter());
            $this->formProfile = $form;
        }

        return $this->formProfile;
    }


    /**
     * @return \Application\Forms\ChangePasswordForm|null
     */
    public function getChangePasswordForm()
    {
        if (!$this->formChangePassword) {
            $form = new \Application\Forms\ChangePasswordForm();
            $form->prepareElements();
            $filters = new \Application\InputFilters\ChangePasswordFormFilters();
            $form->setInputFilter($filters->getInputFilter());
            $this->formChangePassword = $form;
        }

        return $this->formChangePassword;
    }

    /**
     *
     * @return \Application\Models\ProfileModel
     */
    public function getModelProfile()
    {
        if ($this->modelProfile === null) {
            $this->modelProfile = new \Application\Models\ProfileModel($this->getDaoFactory());
        }
        return $this->modelProfile;
    }


}
