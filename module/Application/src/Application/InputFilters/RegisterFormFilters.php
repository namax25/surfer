<?php

namespace Application\InputFilters;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Login
 *
 * @author namax
 *
 */
class RegisterFormFilters implements InputFilterAwareInterface
{

    protected $inputFilter;

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();


            $inputFilter->add($factory->createInput(array(
                'name' => 'email',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^0-9a-z\_\.\@\#\%\+\-\=\{\}\~]+/iu',
                            'replacement' => '#',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'EmailAddress'
                    ),
                ),
            )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
