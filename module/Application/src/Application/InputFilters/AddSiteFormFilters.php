<?php

namespace Application\InputFilters;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * Login
 *
 * @author namax
 *
 */
class AddSiteFormFilters implements InputFilterAwareInterface
{

    protected $inputFilter;

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();


            $inputFilter->add($factory->createInput(array(
                'name' => 'title',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^a-zA-Z0-9а-яА-ЯёЁ\_\-]+/iu',
                            'replacement' => '',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 50,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'url',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^a-zA-Z0-9а-яА-ЯёЁ\_\-\?\=\:\~\.\/]+/iu',
                            'replacement' => '',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 5,
                            'max' => 250,
                        ),
                    ),
                    /*    array(
                            'name' => 'Hostname',
                            'options' => array(
                                'allow' => [\Zend\Validator\Hostname::ALLOW_ALL]
                            ),
                        ),*/
                ),
            )));


            $inputFilter->add($factory->createInput(array(
                'name' => 'time_to_show',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^0-9\.]+/u',
                            'replacement' => '',
                        ),
                    ),
                ),
            )));

            /*            $inputFilter->add($factory->createInput(array(
                            'name' => 'show_interval',
                            'filters' => array(
                                array('name' => 'StripTags'),
                                array('name' => 'StringTrim'),
                                array(
                                    'name' => 'PregReplace',
                                    'options' => array(
                                        'pattern' => '/[^0-9]+/u',
                                        'replacement' => '',
                                    ),
                                ),
                            ),
                            'validators' => array(
                                array(
                                    'name' => 'Between',
                                    'options' => array(
                                        'min' => 0,
                                        'max' => 60000,
                                    ),
                                ),

                            ),

                        )));*/

            $inputFilter->add($factory->createInput(array(
                'name' => 'shows_per_hour',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^0-9]+/u',
                            'replacement' => '',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'Between',
                        'options' => array(
                            'min' => 0,
                            'max' => 100,
                        ),
                    ),

                ),

            )));

            $inputFilter->add($factory->createInput(array(
                'name' => 'is_active',
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array(
                        'name' => 'PregReplace',
                        'options' => array(
                            'pattern' => '/[^0-9]+/u',
                            'replacement' => '',
                        ),
                    ),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array(0, 1)
                        ),
                    ),

                ),

            )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
