<?php

namespace Application\Models;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;

/**
 * Description of ContactsModel
 *
 * @author namax
 */
class SiteModel
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function update(\Entities\Urls $entityUrls, $id)
    {
        return $this->getDaoFactory()->getDaoUrls()->update(
            $entityUrls->toDb(),
            ['id' => $id]
        );
    }

    public function insert(\Entities\Urls $entityUrls)
    {
        if (empty($entityUrls->getUrl())) {
            throw new \Exception(__METHOD__ . " Url can't be empty");
        }
        return $this->getDaoFactory()->getDaoUrls()->insert($entityUrls->toDb());
    }

    public function deleteSite($userId, $id)
    {
        return $this->getDaoFactory()->getDaoUrls()->delete(['id' => $id, "user_id" => $userId]);
    }

    public function getSite($userId, $id)
    {
        $userId = \Helpers\Strings::create()->clearDigits($userId);

        if ($userId <= 0) {
            throw new \Exception(__METHOD__ . " Invalid user id " . $userId);
        }

        return $this->getDaoFactory()->getDaoUrls()->getByIdForUser($userId, $id);
    }


    public function getList($userId)
    {

        $userId = \Helpers\Strings::create()->clearDigits($userId);

        $select = new Select('urls');
        $select->where(['user_id' => $userId]);
        $resultSetPrototype = new ResultSet();
        $paginatorAdapter = new DbSelect(
            $select,
            $this->getDaoFactory()->getDaoUrls()->getTableGateway()->getAdapter(),
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);

        return $paginator;
    }

}
