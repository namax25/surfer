<?php
/**
 * Date: 11/11/14
 * Time: 05:14 PM
 * @author namax
 */

namespace Application\Models;

use \Consts\Table\BillingType\BillingType;

class PartnerModel
{


    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    /**
     * @var \Entities\UrlsViewedEntity
     */
    private $urlViewedEntity = null;


    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function getDataForIndexTable($userData)
    {
        return $this->getDaoFactory()->getTariff()->getAll();

    }


    public function updateViewedUrl($viewedUrlData)
    {
        $this->getUrlViewedEntity()->clear();
        $this->getUrlViewedEntity()->fromArray($viewedUrlData);

        if (!$this->getUrlViewedEntity()->isEmpty()) {
            $this->getUrlViewedEntity()->setDateReport(\Helpers\DateTime::create()->now());
            return $this->getDaoFactory()->getUrlsViewed()->update(
                $this->getUrlViewedEntity()->toDb(),
                ['id' => $this->getUrlViewedEntity()->getId()]
            );
        }

        return false;
    }

    /**
     * @return \Entities\UrlsViewedEntity
     */
    public function getUrlViewedEntity()
    {
        if (is_null($this->urlViewedEntity)) {
            $this->urlViewedEntity = new \Entities\UrlsViewedEntity();
        }
        return $this->urlViewedEntity;
    }


    public function saveUrlReturnedToView($userId, $urlId)
    {
        $this->getUrlViewedEntity()->clear();
        $this->getUrlViewedEntity()->setDateCreated(\Helpers\DateTime::create()->now());
        $this->getUrlViewedEntity()->setUserId($userId);
        $this->getUrlViewedEntity()->setUrlId($urlId);
        $this->getUrlViewedEntity()->generateHash();

        return $this->getDaoFactory()->getUrlsViewed()->insert($this->getUrlViewedEntity()->toDb());
    }

    public function getHashOfViewedUrl($userId, $urlId)
    {
        $this->getUrlViewedEntity()->clear();
        $this->getUrlViewedEntity()->setDateCreated(\Helpers\DateTime::create()->now());
        $this->getUrlViewedEntity()->setUserId($userId);
        $this->getUrlViewedEntity()->setUrlId($urlId);
        $this->getUrlViewedEntity()->generateHash();
        return $this->getUrlViewedEntity()->getHash();
    }

    public function prepareUrlDataToSendToClient($urlData)
    {
        $urlDataForSend = [];

        $urlDataForSend['url'] = $urlData->url;
        $urlDataForSend['time_to_show'] = $urlData->time_to_show;
        $urlDataForSend['referers'] = $urlData->referers;
        $urlDataForSend['hash'] = $urlData->hash;
        return $urlDataForSend;
    }

    public function createRefferal($referralId, $newUserId)
    {
        $referralId = \Helpers\Strings::create()->clearDigits($referralId);
        $newUserId = \Helpers\Strings::create()->clearDigits($newUserId);
        if (!$referralId || !$newUserId) {
            return false;
        }


        $referrals = $this->getRefferals($referralId);
        $parentId = $referrals ? $referrals['parent_id'] : $referralId;

        $data = [];
        $data['parent_id'] = $parentId;
        $data['user_id'] = $newUserId;
        $data['ref_user_id'] = $referralId;
        return $this->getDaoFactory()->getReferrals()->insert($data);
    }

    protected function getRefferals($referralId)
    {

        $result = $this->getDaoFactory()->getReferrals()->getByRefUserId($referralId, 1);
        $referrals = $result->toArray();

        return $referrals ? $referrals[0] : false;
    }


    public function payToUserForViewedUrl($userTariff, $userId, $urlId)
    {
        $billingTypes = $this->getDaoFactory()->getBillingType()->getAll();
        $billingTypes = \Helpers\ArraySort::create()->setColumnAsKey($billingTypes->toArray(), 'id');

        $billingTypeEntity = new \Entities\BillingType();
        $billingTypeEntity->fromDb($billingTypes[BillingType::PAY_FOR_VIEW_URL]);

        $amountToPay = $billingTypeEntity->getPriceByTariff($userTariff);

        $this->getDaoFactory()->getDaoUsers()->addToBalance($amountToPay, $userId);

        $userData = $this->getDaoFactory()->getDaoUsers()->getById($userId);
        $billingHistory = new \Entities\BillingHistory();
        $billingHistory->setCredit($amountToPay)
            ->setUserId($userId)
            ->setDate(\Helpers\DateTime::create()->now())
            ->setBillingTypeId(BillingType::PAY_FOR_VIEW_URL)
            ->setDescription($billingTypeEntity->getName() . " за url " . $urlId)
            ->setBalance($userData->balance);
        return $this->getDaoFactory()->getBillingHistory()->insert($billingHistory->toDb());
    }


} 