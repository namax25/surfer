<?php

namespace Application\Models;





/**
 * Description of ContactsModel
 *
 * @author namax
 */
class UserModel
{

    /**
     *
     * @var \Dao\DaoFactory
     */
    private $daoFactory = null;

    public function __construct(\Dao\DaoFactory $daoFactory)
    {
        $this->setDaoFactory($daoFactory);
    }

    public function getDaoFactory()
    {
        return $this->daoFactory;
    }

    public function setDaoFactory(\Dao\DaoFactory $daoFactory)
    {
        $this->daoFactory = $daoFactory;
    }

    public function getUserProfileByLogin($login)
    {
        return $this->getDaoFactory()->getDaoUsers()->getByLogin($login);
    }

    public function addToBalance($amount, $userId)
    {
        return $this->getDaoFactory()->getDaoUsers()->addToBalance($amount, $userId);
    }


    public function register($email, \Mail\iMail $mail)
    {

        $data = [];
        $data['email'] = $email;
        $data['login'] = $email;
        $data['register_date'] = \Helpers\DateTime::create()->now();
        $data['role'] = 'user';
        $password = \Helpers\Password::create()->getRandomPassword();
        $data['password'] = \Helpers\Password::create()->generate(
            $email,
            $password
        );
        $data['status'] = \Consts\Table\Users\Status::NEWONE;

        $mailParams = new \Mail\MailParams();
        $mailParams
            ->setFromAddress("registration@surf.com")
            ->setToAddresses([$email])
            ->setMessageSubject("регистрация на сайте")
            ->setBodyHtml("<h1>Регистрация:</h2> " . $email . "; " . $password)
            ->setReplyToAddresses(['twh11mail@gmail.com'])
            ->setReturnPath('twh11mail@gmail.com');

//                $mail->send($mailParams);
        $dataForLog = [
            'date' => \Helpers\DateTime::create()->now(),
            'email' => $email,
            'status' => 0,
            'text' => 'Регистрация ' . $email . "; " . $password,
            'message_id' => empty($resultArr['MessageId']) ? null : $resultArr['MessageId'],
            'request_id' => empty($resultArr['ResponseMetadata']['RequestId']) ? null : $resultArr['ResponseMetadata']['RequestId'],
        ];
        $this->getDaoFactory()->getLogEmail()->insert($dataForLog);

        return $this->getDaoFactory()->getDaoUsers()->insert($data);
    }


}
