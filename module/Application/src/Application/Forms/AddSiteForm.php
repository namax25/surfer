<?php

namespace Application\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * LoginForm
 *
 * @author namax
 *
 */
class AddSiteForm extends Form
{

    public function prepareElements()
    {

        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));



        $this->add(
            $this->createElement(
                new Element\Text('title'),
                [
                    'class' => 'form-control'

                ],
                ['label' => 'Название'],
                ['class' => 'control-label']
            )
        );


        /*        $this->add(
                    $this->createElement(
                        new Element\Text('show_interval'),
                        [
                            'class' => 'form-control'
                        ],
                        ['label' => 'Интервал между показами(в секундах)'],
                        ['class' => 'control-label']
                    )
                );*/
        $this->add(
            $this->createElement(
                new Element\Text('shows_per_hour'),
                [
                    'class' => 'form-control'
                ],
                ['label' => 'Количество показов в час'],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Text('url'),
                [
                    'class' => 'form-control'
                ],
                ['label' => 'URL сайта'],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Radio('is_active'),
                [
                ],
                [
                    'label' => 'Активный',
                    'value_options' => [
                        '0' => 'нет',
                        '1' => 'да',
                    ]
                ],
                ['class' => 'radio-inline']
            )
        );


        $listIdentifiers = [
            "1" => "30 секунд (1 кредит - показ)",
            "1.5" => "45 секунд (1,5 кредита - показ)",
            "2" => "60 секунд (2 кредита - показ)",
            "2.5" => "75 секунд (2,5 кредита - показ)",
            "3" => "90 секунд (3 кредита - показ)",
            "3.5" => "105 секунд (3,5 кредита - показ)",
            "4" => "120 секунд (4 кредита - показ)",
            "5" => "150 секунд (5 кредитов - показ)",
            "6" => "180 секунд (6 кредитов - показ)",
            "7" => "210 секунд (7 кредитов - показ)",
            "8" => "240 секунд (8 кредитов - показ)",
            "9" => "270 секунд (9 кредитов - показ)",
            "10" => "300 секунд (10 кредитов - показ)"
        ];


        $this->add(
            $this->createElement(
                new Element\Select('time_to_show'),
                [
                    'class' => 'form-control'
                ],
                [
                    'label' => 'Время показа',
                    'empty_option' => '-- Не выбрано --',
                    'value_options' => $listIdentifiers,
                ],
                ['class' => 'control-label']
            )
        );

        $this->add(
            $this->createElement(
                new Element\Textarea('referers'),
                [
                    'class' => 'form-control'
                ],
                [
                    'label' => 'Http referers',

                ],
                ['class' => 'control-label']
            )
        );


        $this->add(
            $this->createElement(
                new Element\ Submit('submit'),
                [
                    'type' => 'submit',
                    'value' => 'Сохранить',
                    'class' => 'btn btn-default'
                ]
            )
        );
    }

    protected function createElement(
        Element $element,
        array $attributes,
        array $options = [],
        array $labelAttributes = []
    ) {
        $element->setAttributes($attributes);
        if ($options) {
            $element->setOptions($options);
        }
        if ($labelAttributes) {
            $element->setLabelAttributes($labelAttributes);
        }
        return $element;
    }

}
