<?php

namespace Application\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * LoginForm
 *
 * @author namax
 *
 */
class RegisterForm extends Form
{

    public function prepareElements()
    {

        $this->setAttribute('method', 'post');

        $this->add($this->createElement(new Element\Email('email'), [
                'class' => 'form-control',
                'id' => 'email',
                'placeholder' => 'email'
            ], ['label' => 'email'], ['class' => 'control-label']
        ));


        $this->add($this->createElement(new Element\ Submit('submit'), [
                'type' => 'submit',
                'value' => 'Зарегистрироваться',
                'class' => 'btn btn-default'
            ]
        ));
    }

    protected function createElement(
        Element $element,
        array $attributes,
        array $options = [],
        array $labelAttributes = []
    ) {
        $element->setAttributes($attributes);
        if ($options) {
            $element->setOptions($options);
        }
        if ($labelAttributes) {
            $element->setLabelAttributes($labelAttributes);
        }
        return $element;
    }

}
