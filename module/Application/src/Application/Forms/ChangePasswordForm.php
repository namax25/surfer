<?php

namespace Application\Forms;

use Zend\Form\Form;
use Zend\Form\Element;

/**
 * LoginForm
 *
 * @author namax
 *
 */
class ChangePasswordForm extends Form
{

    public function prepareElements()
    {

        $this->setAttribute('method', 'post');


//        $this->add(array(
//            'name' => 'id',
//            'type' => 'Hidden',
//        ));
//        $this->add($this->createElement(new Element\Hidden('id'), []));


        $this->add($this->createElement(new Element\Password('password'), [
                'class' => 'form-control',
                'id' => 'password',
                'placeholder' => 'текущий пароль'
            ], ['label' => 'Пароль'], ['class' => 'control-label']
        ));

        $this->add($this->createElement(new Element\Password('new_password'), [
                'class' => 'form-control',
                'id' => 'new_password',
                'placeholder' => 'новый пароль'
            ], ['label' => 'Новый пароль'], ['class' => 'control-label']
        ));


        $this->add($this->createElement(new Element\Password('new_password_repeat'), [
                'class' => 'form-control',
                'id' => 'new_password_repeat',
                'placeholder' => 'еще раз введите новый пароль'
            ], ['label' => 'Еще раз'], ['class' => 'control-label']
        ));


        $this->add($this->createElement(new Element\ Submit('submit'), [
                'type' => 'submit',
                'value' => 'Сохранить',
                'class' => 'btn btn-default'
            ]
        ));
    }

    protected function createElement(
        Element $element,
        array $attributes,
        array $options = [],
        array $labelAttributes = []
    ) {
        $element->setAttributes($attributes);
        if ($options) {
            $element->setOptions($options);
        }
        if ($labelAttributes) {
            $element->setLabelAttributes($labelAttributes);
        }
        return $element;
    }

//    public function getInputFilterSpecification() {
//        return array(
//            'phone' => array(
//                'required' => true,
//                'filters' => array(
//                    array('name' => 'StripTags'),
//                    array('name' => 'StringTrim'),
//                ),
//                'validators' => array(
//                    array(
//                        'name' => 'Digits',
//                    ),
//                ),
//            )
//        );
//    }
}
