<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $eventManager->attach('route', array($this, 'loadConfiguration'), 2);
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function loadConfiguration(MvcEvent $e)
    {
        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $router = $sm->get('router');
        $request = $sm->get('request');

        $matchedRoute = $router->match($request);
        if (null !== $matchedRoute) {
            $sharedManager->attach(
                'Zend\Mvc\Controller\AbstractActionController',
                'dispatch',
                function ($e) use ($sm) {
                    $sm->get('ControllerPluginManager')->get('AclPlugin')->doAuthorization($e, $sm);
                },
                2
            );
        }
        \Zend\Validator\AbstractValidator::setDefaultTranslator($sm->get('translator'));
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
//            'abstract_factories' => array(),
            'aliases' => array(
                'Zend\Authentication\AuthenticationService' => 'AuthenticationService',
            ),
            'factories' => array(
                'DaoFactory' => "\Dao\DaoFactory",
                'Dao\Users' => function ($sm) {
                    return new \Dao\Users($sm->get('db'));
                },
                'Common\Storage\Auth' => function ($sm) {
                    return new \App\Auth\Storage\Session('sm_session');
                },
                'AuthenticationService' => function ($sm) {
                    $daoFactory = $sm->get('DaoFactory');
                    $dbAuthAdapter = new \App\Auth\DbAdapter($daoFactory->getDaoUsers());
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbAuthAdapter);
                    $storage = $sm->get('Common\Storage\Auth');
                    $authService->setStorage($storage);

                    $idenity = $authService->getIdentity();

                    if (!empty($idenity->id)) {
                        $daoFactory->getDaoUsers()->update(
                            ['last_activity' => \Helpers\DateTime::create()->now()],
                            ['id' => $idenity->id]
                        );
                    }

                    return $authService;
                },
                'log_db' => function ($sm) {
                    $log_db = new \Log\Logger();
                    $processor = new \Log\Processor\Backtrace();
                    $log_db->addProcessor($processor);
                    $mapColumn = [
                        'trace' => 'trace',
                        'timestamp' => 'date',
                        'message' => 'message',
                        'priority' => 'priority'
                    ];
                    $writer = new \Log\Writer\Db($sm->get('db'), "log", $mapColumn);
                    $log_db->addWriter($writer);
                    return $log_db;
                },
                'MailService' => function ($sm) {
                    $config = $sm->get('config');
                    $amazon_ses = $config['amazon_ses'];
                    $mailService = new \Mail\GatewayProviders\AmazonSes(array(
                        'key' => $amazon_ses['aws_access_key_id'],
                        'secret' => $amazon_ses['aws_secret_access_key'],
                        'aws_access_key_id' => $amazon_ses['aws_access_key_id'],
                        'aws_secret_access_key' => $amazon_ses['aws_secret_access_key'],
                        'region' => $amazon_ses['region'],
                    ));
                    return $mailService;
                },
                'Redis' => function ($sm) {
                    $redis = new \Redis();

                    if (!$redis) {
                        throw new \Exception(__METHOD__ . " Can not load redis");
                    };

                    if (!$redis->connect('127.0.0.1')) {
                        throw new \Exception(__METHOD__ . " Can not connect to redis");
                    }
                    return $redis;
                },

            ),
            'invokables' => array(//                'my_auth_service' => 'Zend\Authentication\AuthenticationService',
            ),
            'services' => array(),
//            'shared' => array(),
        );
    }
}

