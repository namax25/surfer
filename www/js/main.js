jQuery(function ($) {

    'use strict';
    /* 
     * Globals
     */

    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: 'Предыдущий месяц',
        nextText: 'Следующий месяц',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь',
            'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
        dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Вых',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
    var utils = {
        consoleByGroup: function (groupName, data) {
            if (data) {
                $.each(data, function (key, val) {
                    console.group(groupName);
                    console.log(val);
                    console.groupEnd();
                });
            }
        },
        getLocalTimezone: function () {
            var tz = jstz.determine();
            return tz.name();
        },
    };
    var alerts = {
        isClearSatusTimerOn: false,
        init: function () {
            this.cacheElements();
        },
        cacheElements: function () {
            this.$statusMessages = $("#statusMessages");
            //templates
            this.$successAlertTemplate = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Выполненно успешно!</strong><br />__MSG__</div>';
            this.$infoAlertTemplate = '<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Информация.</strong><br />__MSG__</div>';
            this.$warnAlertTemplate = '<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Внимание!</strong><br />__MSG__</div>';
            this.$errAlertTemplate = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Ошибка!</strong><br />__MSG__</div>';
        },
        clearMessagesByTimer: function () {
            if (this.isClearSatusTimerOn === false) {
                if (this.$statusMessages.html()) {
                    this.isClearSatusTimerOn = true;
                    this.$statusMessages.delay(10000).fadeOut(600, function () {
                        alerts.isClearSatusTimerOn = false;
                        alerts.$statusMessages.empty();
                    });
                }
            }
        },
        err: function (msg) {
            if (msg.errors) {
                alerts.printAlert(msg.errors, this.$errAlertTemplate);
            }

        },
        success: function (msg) {
            if (msg.success) {
                alerts.printAlert(msg.success, this.$successAlertTemplate);
            }

        },
        warn: function (msg) {
            if (msg.warn) {
                alerts.printAlert(msg.warn, this.$warnAlertTemplate);
            }

        },
        info: function (msg) {
            if (msg.info) {
                alerts.printAlert(msg.info, this.$infoAlertTemplate);
            }

        },
        printAllAlerts: function (messages) {
            this.$statusMessages.show();
            this.success(messages);
            this.info(messages);
            this.warn(messages);
            this.err(messages);
            this.clearMessagesByTimer();
//            alerts.isClearSatusTimerOn = true;
//            this.$statusMessages.delay(10000).fadeOut(600, function() {
//                alerts.isClearSatusTimerOn = false;
//                alerts.$statusMessages.empty();
//            });
        },
        printAlert: function (messages, template) {
            if (messages) {
                if (messages.length > 1) {
                    var div = $('<div></div>');
                    $.each(messages, function (key, val) {
                        div.append("<p>" + val + "<\p>");
                    });
                    var alertHtml = template.replace(/__MSG__/g, div.html());
                    alerts.$statusMessages.append(alertHtml);
                } else {
                    var alertHtml = template.replace(/__MSG__/g, messages[0]);
                    alerts.$statusMessages.append(alertHtml);
                }
            }
        }
    };
    var formContacts = {
        init: function () {

            if ($('#formContactsEdit').length === 0) {
                return false;
            }
            this.cacheElements();
            this.bindEvents();
            this.datepickerInit();
        },
        cacheElements: function () {

            this.$form = $("#formContactsEdit");
            this.$datepicker = this.$form.find("#datepicker");
            //address
            this.$addressAdd = this.$form.find("#addressAdd");
            this.$addressGroup = this.$form.find("#addresses");
            this.$addressTemplate = this.$form.find('#address_template span').data('template');
            //phone
            this.$phoneAdd = this.$form.find("#phoneAdd");
            this.$phonesGroup = this.$form.find("#phones");
            this.$phoneTemplate = this.$form.find('#phone_template span').data('template');
            //email
            this.$emailAdd = this.$form.find("#emailAdd");
            this.$emailsGroup = this.$form.find("#emails");
            this.$emailTemplate = this.$form.find('#email_template span').data('template');
            //comments
            this.$commentAdd = this.$form.find("#commentAdd");
            this.$commentsGroup = this.$form.find("#comments");
            this.$commentTemplate = this.$form.find('#comment_template span').data('template');
            //templates
            this.$beforeElement = '<div class="form-group"><div class="input-group">';
            this.$deleteButtonTemplate = ' <span class="input-group-btn"><button type="button" class="btn btn-default remove"   title="Удалить"><span class="glyphicon glyphicon-remove text-danger"></span></button>';
            this.$afterElement = this.$deleteButtonTemplate + '  </span></div>';
        },
        bindEvents: function () {

            this.$addressAdd.on('click', this.addAddressInput);
            this.$phoneAdd.on('click', this.addPhoneInput);
            this.$emailAdd.on('click', this.addEmailInput);
            this.$commentAdd.on('click', this.addCommentInput);
            $(this.$form).on('click', ".remove", this.removeInput);
        },
        datepickerInit: function () {
            this.$datepicker.datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
            $("#anim").change(function () {
                this.$datepicker.datepicker("option", "showAnim", "slideDown");
            });
        },
        removeInput: function () {
            $(this).parent().parent().parent().remove();
        },
        addAddressInput: function () {
            var template = formContacts.$addressTemplate.replace(/__index__/g, formContacts.$form.find('.address').length);
            formContacts.$addressGroup.append(formContacts.$beforeElement + template + formContacts.$afterElement);
        },
        addPhoneInput: function () {
            var template = formContacts.$phoneTemplate.replace(/__index__/g, formContacts.$form.find('.phone').length);
            formContacts.$phonesGroup.append(formContacts.$beforeElement + template + formContacts.$afterElement);
        },
        addEmailInput: function () {
            var template = formContacts.$emailTemplate.replace(/__index__/g, formContacts.$form.find('.email').length);
            formContacts.$emailsGroup.append(formContacts.$beforeElement + template + formContacts.$afterElement);
        },
        addCommentInput: function () {
            var template = formContacts.$commentTemplate.replace(/__index__/g, formContacts.$form.find('.comment').length);
            formContacts.$commentsGroup.append(formContacts.$beforeElement + template + formContacts.$afterElement);
        }

    };
    var calendar = {
        lastEventFromUser: null,
        isLastEventStick: true,
        init: function () {
            if ($('#calendar').length === 0) {
                return false;
            }
            var $this = this;
            this.cacheElements();
            this.bindEvents();
            this.calendarInit($this);
        },
        cacheElements: function () {
            this.$calendar = $('#calendar');
            this.$datetimepicker1 = $('#datetimepicker1');
            this.$datetimepicker2 = $('#datetimepicker2');
            this.$datetimepickerReminderDate = $('#datetimepickerReminderDate');
            this.$allDayEvent = $('#allDay');
            this.$remindPeriodSelect = $('#remindPeriodSelect');
            this.$modalEventAdd = $("#modalContactAddEvent");
            this.$eventTitle = this.$modalEventAdd.find("#eventTitle");
            this.$notifyBySms = this.$modalEventAdd.find("#notifyBySms");
            this.$notifyByEmail = this.$modalEventAdd.find("#notifyByEmail");
            this.$eventTitleSaveButton = this.$modalEventAdd.find("#eventTitleSaveButton");
            this.$eventTitleDeleteButton = this.$modalEventAdd.find("#eventTitleDeleteButton");
        },
        bindEvents: function () {
            this.$eventTitleSaveButton.on('click', this.changeEvent);
            this.$eventTitleDeleteButton.on('click', this.deleteEvent);
            this.$datetimepicker1.datetimepicker(
                {
                    language: 'ru'
                });
            this.$datetimepicker2.datetimepicker(
                {
                    language: 'ru'
                });
            this.$datetimepicker1.on("dp.change", function (e) {
                calendar.$datetimepicker2.data("DateTimePicker").setMinDate(e.date.subtract('days', 1));
            });
            this.$datetimepicker2.on("dp.change", function (e) {
                calendar.$datetimepicker1.data("DateTimePicker").setMaxDate(e.date);
            });
            this.$datetimepickerReminderDate.datetimepicker(
                {
                    language: 'ru'
                });
            this.$datetimepickerReminderDate.on("dp.change", function (e) {
                calendar.$datetimepickerReminderDate.data("DateTimePicker");
            });
        },
        deleteEvent: function () {
            calendar.$calendar.fullCalendar('removeEvents', calendar.lastEventFromUser.id);
            calendar.$modalEventAdd.modal('hide');
            var dataSend = {
                id: calendar.lastEventFromUser.id
            };
            $.post("/app/calendar/delete-event", dataSend, function (data) {
                alerts.printAllAlerts(data.msg);
            }, "json");
        },
        changeEvent: function () {

            var title = calendar.$eventTitle.val();
            calendar.$eventTitle.val(null);

            calendar.lastEventFromUser.start = calendar.$datetimepicker1.data("DateTimePicker").getDate().toDate();
            calendar.lastEventFromUser.end = calendar.$datetimepicker2.data("DateTimePicker").getDate().toDate();
            calendar.lastEventFromUser.allDay = calendar.$allDayEvent.prop("checked");
            calendar.lastEventFromUser.notify_date = calendar.$datetimepickerReminderDate.data("DateTimePicker").getDate().toDate();
            calendar.lastEventFromUser.how_often_notify = calendar.$remindPeriodSelect.val();
            calendar.lastEventFromUser.notifyByEmail = calendar.$notifyByEmail.prop("checked") ? 1 : 0;
            calendar.lastEventFromUser.notifyBySms = calendar.$notifyBySms.prop("checked") ? 1 : 0;


            if (calendar.$eventTitle.data("is_update")) {
                $.extend(calendar.lastEventFromUser, {
                    title: title
                });
//                     console.log('changeEvent');

                calendar.$calendar.fullCalendar('updateEvent', calendar.lastEventFromUser);
                calendar.eventUpdate(calendar.lastEventFromUser, calendar.lastEventFromUser.allDay);
            } else {
//                console.log("renderEvent");
                $.extend(calendar.lastEventFromUser, {
                    id: 0,
                    title: title
                });
                calendar.isLastEventStick = true;
                var event = calendar.lastEventFromUser;
                calendar.eventUpdate(event, event.allDay);
            }
            calendar.$modalEventAdd.modal('hide');
        },
        eventUpdate: function (event, allDay) {
//            console.log('eventUpdate');
//            console.log(event);
            var dataSend = {
                id: event.id,
                start: moment.tz(moment(event.start).format("YYYY-MM-DD HH:mm:ss"), $.cookie("timezone")).format(), //Math.round(event.start.getTime() / 1000),
                end: 0,
                allDay: allDay ? 1 : 0,
                notify_by_mail: event.notifyByEmail ? 1 : 0,
                notify_by_sms: event.notifyBySms ? 1 : 0,
                title: event.title
            };
            if (event.end) {
                dataSend.end = moment.tz(moment(event.end).format("YYYY-MM-DD HH:mm:ss"), $.cookie("timezone")).format(); //Math.round(event.end.getTime() / 1000);
            }
            if (event.how_often_notify) {
                dataSend.how_often_notify = event.how_often_notify;
            }

            if (event.notify_date) {
                dataSend.notify_date = moment.tz(moment(event.notify_date).format("YYYY-MM-DD HH:mm:ss"), $.cookie("timezone")).format(); //Math.round(event.notify_date.getTime() / 1000);
            }


//            console.log(calendar.$notifyBySms.prop("checked"));
//            console.log(calendar.$notifyByEmail.prop("checked"));

//            console.log("dataSend");
//            console.log(dataSend);

            $.post("/app/calendar/update-event", dataSend, function (data) {
                if (data.payload.last_event_id) {
                    calendar.$calendar.fullCalendar('refetchEvents');
                }
                alerts.printAllAlerts(data.msg);
            }, "json");
        },
        calendarInit: function ($this) {
            this.$calendar.fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                    'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                monthNamesShort: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь',
                    'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                dayNames: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                dayNamesShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                axisFormat: "H:mm",
                timeFormat: 'H:mm{ - H:mm}',
                allDayText: "Весь день",
                buttonText: {
                    today: 'сегодня',
                    month: 'месяц',
                    week: 'неделя',
                    day: 'день'
                },
                firstDay: 1,
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {
                    $this.$eventTitle.data("is_update", 0);
                    $this.$eventTitle.val("");
                    calendar.$datetimepicker1.data("DateTimePicker").setDate(start);
                    calendar.$datetimepicker2.data("DateTimePicker").setDate(end);
                    calendar.$allDayEvent.prop("checked", allDay);
                    calendar.lastEventFromUser = {
                        start: start,
                        end: end,
                        allDay: allDay
                    };
                    calendar.$remindPeriodSelect.val('0');
                    calendar.$datetimepickerReminderDate.data("DateTimePicker").setDate(start);
                    $this.$eventTitleDeleteButton.hide();
                    $this.$modalEventAdd.modal();
                    calendar.$calendar.fullCalendar('unselect');
                },
                eventClick: function (calEvent, jsEvent, view) {
//                    console.log("eventClick");
//                    console.log(calEvent);

                    calendar.lastEventFromUser = calEvent;
                    calendar.$datetimepicker1.data("DateTimePicker").setDate(calEvent.start);
                    if (calEvent.end) {
                        calendar.$datetimepicker2.data("DateTimePicker").setDate(calEvent.end);
                    } else {
                        calendar.$datetimepicker2.data("DateTimePicker").setDate(calEvent.start);
                    }

                    if (calEvent.notify_date) {
                        calendar.$datetimepickerReminderDate.data("DateTimePicker").setDate(calEvent.notify_date);
                    } else {
                        calendar.$datetimepickerReminderDate.data("DateTimePicker").setDate(null);

                    }

                    calendar.$remindPeriodSelect.val(calEvent.how_often_notify);
                    calendar.$allDayEvent.prop("checked", calEvent.allDay);
                    console.log(calEvent);

                    if (calEvent.notifyByEmail) {
                    }
                    calendar.$notifyByEmail.prop("checked", calEvent.notifyByEmail);
                    calendar.$notifyBySms.prop("checked", calEvent.notifyBySms);

                    $this.$eventTitle.val(calEvent.title);
                    $this.$eventTitle.data("is_update", 1);
                    $this.$eventTitleDeleteButton.show();
                    $this.$modalEventAdd.modal();
                },
                eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc) {
                    calendar.lastEventFromUser = event;
                    calendar.eventUpdate(event, allDay);
                },
                eventResize: function (event, dayDelta, minuteDelta, revertFunc) {

                    calendar.lastEventFromUser = event;
//                    var dataSend = {
//                        id: event.id,
//                        start: Math.round(event.start.getTime() / 1000),
//                        end: 0,
//                        dayDelta: dayDelta,
//                        minuteDelta: minuteDelta
//                    };
//
//                    if (event.end) {
//                        dataSend.end = Math.round(event.end.getTime() / 1000);
//                    }
                    calendar.eventUpdate(event);
                },
                editable: true,
                events: function (start, end, callback) {
                    $.ajax({
                        url: '/app/calendar/all-events',
                        dataType: 'json',
                        data: {
                            start: Math.round(start.getTime() / 1000),
                            end: Math.round(end.getTime() / 1000)
                        },
                        success: function (doc) {
                            var events = [];
                            $(doc.payload).each(function () {
                                var notify_date_local = null;
                                if (this.notify_date) {
                                    notify_date_local = new Date(moment.tz(this.notify_date, 'Etc/UTC').tz($.cookie("timezone")).format("YYYY-MM-DD HH:mm:ss"));
                                }
                                events.push({
                                    id: this.id,
                                    title: this.title,
                                    start: moment.tz(this.startDate, 'Etc/UTC').tz($.cookie("timezone")).format(),
                                    end: moment.tz(this.endDate, 'Etc/UTC').tz($.cookie("timezone")).format(),
                                    allDay: Boolean(parseInt(this.allday)),
                                    how_often_notify: parseInt(this.how_often_notify),
                                    notify_date: notify_date_local,
                                    notifyByEmail: Boolean(parseInt(this.by_email)),
                                    notifyBySms: Boolean(parseInt(this.by_sms)),
                                });
                            });
                            callback(events);
                        }
                    });
                }

            });
        }


    };
    var clockAndDate = {
        timezone: null,
        init: function () {
            if ($('#clocknow').length === 0) {
                return false;
            }
            var $this = this;
            this.updateTimezone();
            this.cacheElements();
            if (!clockAndDate.timezone) {
                this.timedUpdate();
            }
        },
        cacheElements: function () {
            this.$clock = $('#clocknow');
            this.$date = $('#datenow');
        },
        timedUpdate: function () {
            moment.lang('ru');
            if (clockAndDate.timezone === null && $.cookie("timezone")) {
                clockAndDate.timezone = $.cookie("timezone");
            }

            var format = 'H:mm';
            if (clockAndDate.timezone) {
                this.$clock.html(moment().tz(clockAndDate.timezone).format(format));
            } else {
                this.$clock.html(moment().format(format));
            }
            var format = 'dddd, DD MMM YYYY ZZ';
            if (clockAndDate.timezone) {
                this.$date.html(moment().tz(clockAndDate.timezone).format(format));
            } else {
                this.$date.html(moment().format(format));
            }
        },
        updateTimezone: function () {
            var tz = jstz.determine();
            $.get('/app/profile/timezone', {timezone: tz.name()}, function (data) {

                if (data.success) {
                    clockAndDate.timezone = data.payload.timezone;
                }

                alerts.printAllAlerts(data.msg);
            }, "json");
        }


    };
    var newEvents = {
        init: function () {
            if ($('#new-events-box').length === 0) {
                return false;
            }

            var $this = this;
            this.cacheElements();
            this.bindEvents();
        },
        cacheElements: function () {
            this.$newEventsBox = $('#new-events-box');
            this.$buttonAllRead = $('#newEventsAllRead');
        },
        bindEvents: function () {
            this.$buttonAllRead.on('click', this.allReadEvents);
        },
        allReadEvents: function () {
            $.post("/app/calendar/all-read-events", function (data) {
                alerts.printAllAlerts(data.msg);

            }, "json");

            newEvents.$newEventsBox.delay(100).fadeOut(800, function () {
                newEvents.$newEventsBox.empty();
            });


        }
    };

    clockAndDate.init();
    alerts.init();
    formContacts.init();
    calendar.init();
    newEvents.init();
    function timedUpdate() {
        alerts.clearMessagesByTimer();
        clockAndDate.timedUpdate();
        setTimeout(timedUpdate, 1000);
    }

    timedUpdate();
    $('#testresult').click(function () {

        $.post('/app/test/any', {term: date.toISOString()}, function (data) {
//            console.log(data.payload);
            alerts.printAllAlerts(data.msg);
        }, "json");
    });
//    $("#tags").autocomplete({
//        source: function(request, response) {
//            $.get('http://localhost:8085/app/profile/timezone', {term: request.term}, function(data) {
//                console.log(data.payload);
//                alerts.printAllAlerts(data.msg);
//                return response([data.payload]);
//            }, "json");
//        },
//        minLength: 2
//    });


});




