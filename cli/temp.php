<?php

system("clear");
mb_internal_encoding('UTF-8');
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '256M');
set_time_limit(0);

defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__ . '/../'));

//Подключение автозагрузки классов
require_once ROOT_PATH . '/vendor/autoload.php';
require_once ROOT_PATH . '/cli/utils/simple_html_dom.php';
require_once ROOT_PATH . '/cli/utils/plivo.php';


//Конфигурация базы
$dbConfigs = new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/global.php');

//Подключение локального конфига базы
$localConfig = ROOT_PATH . '/cli/config/db/local.php';
if (file_exists($localConfig)) {
    $dbConfigs->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/local.php'));
}

$db = new Zend\Db\Adapter\Adapter($dbConfigs->main_db->toArray());


$log_cli = prepareLog();
//------------------------------------------------------------------------------
$log_cli->warn("Memory used: " . mem());
//------------------------------------------------------------------------------
main();
//------------------------------------------------------------------------------
$log_cli->warn("Memory used: " . mem());

//------------------------------------------------------------------------------

function main()
{
    global $log_cli, $db;

    $auth_id = 'MAYZZKYMQXODM2NWUWMZ';
    $auth_token = 'YjQyYWU3YzVlZjY3YzJmZTQ1MzA0N2MxZGVlM2Jh';
    $p = new RestAPI($auth_id, $auth_token);

    // Send a message
//    $params = array(
//        'src' => '12032901389',
//        'dst' => '18096606646',
//        'text' => "Не обращай внимания. На тебя высылать дешевле",
//        'type' => 'sms',
//    );
//    $response = $p->send_message($params);
//    var_dump($response);
    $plivo = new \Sms\GatewayProviders\Plivo('MAYZZKYMQXODM2NWUWMZ', 'YjQyYWU3YzVlZjY3YzJmZTQ1MzA0N2MxZGVlM2Jh');

    $text = "Не обращай внимания. На тебя высылать дешевле";
    $response = $plivo->send("12032901389", "79324719996", $text);
    var_dump($response);
    $log_cli->warn("Memory used: " . mem());
}

function updateHashInGrabbersAdsFull()
{
    global $log_cli, $db;

    $daoGrabbersAdsFull = new \Dao\GrabbersAdsFull($db);

    while ($ads = $daoGrabbersAdsFull->getImportedAdsWithEmptyHash()) {

        foreach ($ads as $ad) {
            $hash = \Helpers\Hash::create()->fromArr($ad, [
                    'phone',
                    'price',
                    'street',
                    'building',
                    'corpus',
                    'metro',
                    'period',
                    'distance',
                    'transport',
                    'rooms',
                    'floor',
                    'floors',
                    'client_commission'
                ]);
            $log_cli->log(\Log\Logger::OK, "Update hash for " . $ad['id']);
            $daoGrabbersAdsFull->update(['hash' => $hash], ['id' => $ad['id']]);
        }
    }
}

function responseHandlerCallbackForList($info, $output, $error)
{
    var_dump($error);
    echo $output;
}

function testSplContainers()
{
    global $log_cli;
    $log_cli->warn("Memory used: " . mem());

    $s = new SplQueue();
//    $s = new SplDoublyLinkedList();
//    $s = new SplObjectStorage();
//    $s = new intPq();
//    $s = [];
    for ($i = 1; $i < 3; $i++) {
        $s->enqueue(new Proxy("1.1.1." . $i, $i));
//        $s->push(new Proxy("1.1.1.1", $i));
//        $s->attach(new Proxy("1.1.1.1", $i));
//            $s->insert(new Proxy("1.1.1.1", $i), $i);
//        $s[] = ['url' => "1.1.1.1", 'port' => $i];
    }
    var_dump($s->offsetGet(1));


    $p = $s->dequeue();
    var_dump($p);

    $s->enqueue($p);
    var_dump($s->offsetGet(1));

//    $s->rewind();
//    do {
//        $p = $s->current();
//        var_dump($s->key(), $s->current(), $s->isEmpty());
//        $s->next();
//        $p->incErr();
//    } while ($s->valid());
//    $s->rewind();
//    do {
//        var_dump($s->key(), $s->current(), $s->isEmpty());
//        $s->next();
//    } while ($s->valid());
    $log_cli->warn("Memory used: " . mem());
}

function foo()
{
    throw new Exception("hello");
}

class intPq extends SplPriorityQueue
{

    public function compare($priority1, $priority2)
    {
        if ($priority1 === $priority2) {
            return 0;
        }
        return $priority1 < $priority2 ? 1 : -1;
    }
}

class Proxy
{

    private $url = null;
    private $port = null;
    private $error = 0;

    public function __construct($url, $port)
    {
        $this->url = $url;
        $this->port = $port;
    }

    public function incErr()
    {
        $this->error++;
    }
}

//------------------------------------------------------------------------------

function prepareLog()
{
    $processor = new \Log\Processor\Backtrace();
    $formatter_cli = new \Log\Formatter\ColorCli();
    $log_cli = new \Log\Logger();
    $writerStream = new \Zend\Log\Writer\Stream('php://output');
    $writerStream->setFormatter($formatter_cli);
    $log_cli->addProcessor($processor);
    $log_cli->addWriter($writerStream);
    return $log_cli;
}

function mem()
{
    $size = memory_get_usage(true);
    $unit = array('b', 'kb', 'mb', 'gb', 'tb', 'pb');
    return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
}
