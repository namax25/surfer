<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'Dao\Users' => function ($sm) {
                return new \Dao\Users($sm->get('db'));
            },
            'DaoFactory' => "\Dao\DaoFactory",
            'Settings' => function ($sm) {
                $settings = new \Dao\Settings($sm->get('db'));

                return $settings->getSettings();
            },
            'log_db' => function ($sm) {
                $log_db = new \Log\Logger();
                $processor = new \Log\Processor\Backtrace();
                $log_db->addProcessor($processor);
                $mapColumn = [
                    'trace' => 'trace',
                    'timestamp' => 'date',
                    'message' => 'message',
                    'priority' => 'priority'
                ];
                $writer = new \Log\Writer\Db($sm->get('db'), "log", $mapColumn);
                $log_db->addWriter($writer);
                return $log_db;
            },
            'Redis' => function ($sm) {
                $redis = new \Redis();

                if (!$redis) {
                    throw new \Exception(__METHOD__ . " Can not load redis");
                };

                if (!$redis->connect('127.0.0.1')) {
                    throw new \Exception(__METHOD__ . " Can not connect to redis");
                }
                return $redis;
            },
        ),
    ),
    'cookie_path' => array(
        "multi" => function () {
            return realpath(
                ROOT_PATH .
                DIRECTORY_SEPARATOR . 'tmp' .
                DIRECTORY_SEPARATOR . 'cookies'
            );
        }
    ),
    'tesseract_path' => "/usr/local/bin/tesseract",
);
