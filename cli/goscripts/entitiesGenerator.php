<?php

/**
 * Date: 11/28/14
 * Time: 04:09 PM
 * @author namax
 */
class EntitiesGenerator
{
    protected $fields = [];
    protected $fileHandler = null;

    const SPACES_IN_TAB = 4;
    const TABLE_FIELDS_SEPARATOR = "_";


    public function generate(array $fields, $className)
    {
        try {

            $this->createPhpFile($className);
            $this->addClassCaption($className);
            $this->openBrace();
            $this->addFields($fields);
            $this->addEmptyLine();
            $this->addFromArrayMethod($fields, 1);
            $this->addEmptyLine();
            $this->addClearMethod($fields, 1);
            $this->addEmptyLine();
            $this->addToArrayMethod($fields, 1);
            $this->addEmptyLine();
            $this->addToDbMethod($fields, 1);
            $this->addEmptyLine();
            $this->addFromDbMethod($fields, 1);
            $this->addEmptyLine();
            $this->addGettersAndSetters($fields, 1);
            $this->addEmptyLine();

            $this->closeBrace();
        } finally {
            fclose($this->fileHandler);
        }


    }


    protected function createPhpFile($className)
    {
        $this->fileHandler = fopen($className . ".php", "w+");
        $this->writeToFile("<?php\n\n");
        $this->writeToFile("namespace Entities;\n\n");


        if ($this->fileHandler === false) {
            throw new \Exception(__METHOD__ . " Unable to create a file " . $className);
        }

    }


    protected function addClassCaption($className)
    {
        $this->writeToFile("class " . $className . " extends \Entities\AbstractEntity" . "\n");

    }

    protected function addFields($fields)
    {

        foreach ($fields as $field) {
            $this->writeToFile("    protected $" . $this->convertFieldNameToPsr($field) . " = null;\n");
        }
    }

    protected function addEmptyLine()
    {
        $this->writeToFile("\n");
    }

    protected function openBrace($offsetInSpaces = 0)
    {
        $this->writeToFile($this->generateSpaces($offsetInSpaces) . "{\n");
    }

    protected function closeBrace($offsetInSpaces = 0)
    {
        $this->writeToFile($this->generateSpaces($offsetInSpaces) . "}\n");
    }

    protected function generateSpaces($spaces = 0)
    {
        return $spaces > 0 ? str_repeat(" ", $spaces * self::SPACES_IN_TAB) : "";
    }

    protected function writeToFile($string)
    {
        if (!fwrite($this->fileHandler, $string)) {
            throw new \Exception(__METHOD__ . " Unable to write to a file");
        }
    }


    protected function addFunctionSignature($methodVisibility, $funcName, $offset = 0, array $args = [])
    {
        $this->writeToFile(
            $this->generateSpaces($offset) . $methodVisibility . " function " .
            $funcName . "(" . $this->argsToStr($args) . ")" . "\n"
        );
    }


    protected function getMethodCall($methodName, array $args = [])
    {
        return '$this->' . $methodName . "(" . $this->argsToStr($args) . ");";
    }

    protected function getPropertyCall($fieldName)
    {
        return '$this->' . $this->convertFieldNameToPsr($fieldName);
    }

    protected function argsToStr(array $args = [])
    {
        return $args ? implode(", ", $args) : "";
    }

    protected function getMethodName($fieldName, $prefix = "", $suffix = "")
    {
        $result = $this->convertFieldNameToPsr($fieldName);
        return $prefix ? $prefix . ucfirst($result) . ucfirst($suffix) : $result . ucfirst($suffix);
    }

    protected function getPropertyName($fieldName)
    {
        return $this->convertFieldNameToPsr($fieldName);
    }

    protected function convertFieldNameToPsr($fieldName)
    {
        $namesArr = [];
        if (strpos($fieldName, self::TABLE_FIELDS_SEPARATOR) !== false) {
            $namesArr = explode(self::TABLE_FIELDS_SEPARATOR, $fieldName);
        }

        $result = "";
        if (empty($namesArr)) {
            $result = $fieldName;
        } else {
            foreach ($namesArr as $name) {
                $result .= ucfirst($name);
            }
        }

        return lcfirst($result);

    }


    protected function addFromArrayMethod($fieldNames, $offset = 0)
    {

        $this->addFunctionSignature("public", "fromArray", $offset, ['$data']);
        $this->openBrace($offset);

        foreach ($fieldNames as $fieldName) {
            $this->writeToFile($this->generateSpaces($offset + 1));
            $this->writeToFile('if (isset($data[\'' . $fieldName . '\'])) {' . "\n");
            $this->writeToFile($this->generateSpaces($offset + 2));
            $this->writeToFile(
                $this->getMethodCall($this->getMethodName($fieldName, 'set'), ['$data[\'' . $fieldName . '\']']) . "\n"
            );
            $this->closeBrace($offset + 1);
        }

        $this->closeBrace($offset);

    }


    protected function addClearMethod($fieldNames, $offset = 0)
    {
        $this->addFunctionSignature("public", "clear", $offset);
        $this->openBrace($offset);
        foreach ($fieldNames as $fieldName) {
            $this->writeToFile($this->generateSpaces($offset + 1));
            $this->writeToFile($this->getPropertyCall($fieldName) . " = null;\n");
        }
        $this->writeToFile($this->generateSpaces($offset + 1) . 'return $this;' . "\n");
        $this->closeBrace($offset);
    }


    protected function addToArrayMethod($fieldNames, $offset = 0)
    {
        $this->addFunctionSignature("public", "toArray", $offset);
        $this->openBrace($offset);

        $this->writeToFile($this->generateSpaces($offset + 1) . '$data = [];' . "\n");

        foreach ($fieldNames as $fieldName) {
            $this->writeToFile($this->generateSpaces($offset + 1));
            $this->writeToFile(
                '$data[\'' . $fieldName . '\'] = ' .
                $this->getMethodCall($this->getMethodName($fieldName, 'get')) . "\n"
            );
        }
        $this->writeToFile($this->generateSpaces($offset + 1) . 'return $data;' . "\n");
        $this->closeBrace($offset);
    }

    protected function addToDbMethod($fieldNames, $offset = 0)
    {
        $this->addFunctionSignature("public", "toDb", $offset);
        $this->openBrace($offset);
        $this->writeToFile($this->generateSpaces($offset + 1) . 'return $this->toArrayNoNull();' . "\n");
        $this->closeBrace($offset);
    }


    protected function addFromDbMethod($fieldNames, $offset = 0)
    {
        $this->addFunctionSignature("public", "fromDb", $offset, ['$data']);
        $this->openBrace($offset);
        $this->writeToFile($this->generateSpaces($offset + 1) . '$this->fromArray($data);' . "\n");
        $this->writeToFile($this->generateSpaces($offset + 1) . 'return $this;' . "\n");
        $this->closeBrace($offset);
    }

    protected function addGettersAndSetters($fieldNames, $offset = 0)
    {

        foreach ($fieldNames as $fieldName) {

            $this->addGetter($fieldName, $offset);
            $this->addEmptyLine();
            $this->addSetter($fieldName, $offset);
            $this->addEmptyLine();
        }


    }

    protected function addGetter($fieldName, $offset = 0)
    {
        $this->addFunctionSignature("public", $this->getMethodName($fieldName, 'get'), $offset);
        $this->openBrace($offset);
        $this->writeToFile(
            $this->generateSpaces($offset + 1) . 'return ' . $this->getPropertyCall($fieldName) . ";\n"
        );
        $this->closeBrace($offset);
    }

    protected function addSetter($fieldName, $offset = 0)
    {
        $param = '$' . $this->convertFieldNameToPsr($fieldName);
        $this->addFunctionSignature("public", $this->getMethodName($fieldName, 'set'), $offset, [$param]);
        $this->openBrace($offset);
        $this->writeToFile(
            $this->generateSpaces($offset + 1) . $this->getPropertyCall($fieldName) . " = " . $param . ";\n"
        );
        $this->writeToFile(
            $this->generateSpaces($offset + 1) . 'return $this' . ";\n"
        );
        $this->closeBrace($offset);
    }


}

$en = new \EntitiesGenerator();

$fields = [
    'id',
    'user_id',
    'phone',
];

$en->generate($fields, "PhonesEntity");
