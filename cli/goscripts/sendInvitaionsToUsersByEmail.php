<?php

/**
 *
 * @author namax
 */
class SendInvitaionsToUsersByEmail
{

    private $template = null;
    private $logCli = null;

    /**
     *
     * @var  \Dao\Users
     */
    private $daoUsers = null;

    /**
     *
     * @var  \Dao\LogEmail
     */
    private $daoLogEmail = null;

    /**
     *
     * @var  \Dao\LogSms
     */
    private $daoLogSms = null;

    /**
     *
     * @var \Mail\iMail
     */
    protected $mailService = null;

    /**
     *
     * @var \Sms\iSms
     */
    protected $smsService = null;

    public function __construct(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $this->setServiceManager($serviceManager);
        $this->daoUsers = new \Dao\Users($serviceManager->get('main_db'));
        $this->daoLogEmail = new \Dao\LogEmail($serviceManager->get('main_db'));
        $this->daoLogSms = new \Dao\LogSms($serviceManager->get('main_db'));
    }

    public function run()
    {

        $templatePath = ROOT_PATH . "/mailTemplates/userInvitation.html";
        $this->readTemplate($templatePath);

        $amount = 1;
        $offset = 0;


        while ($users = $this->daoUsers->getByRange($amount, $offset)) {
            if ($users->count() == 0) {
                break;
            }
            foreach ($users as $user) {
                $first = mt_rand(10000000, 99999999);
//                $second = mt_rand(10000000, 99999999);

                $this->getLogCli()->info("User: id " . $user->id);
                $password = \Helpers\Password::create()->generate(
                    $user->id, $user->login, $first
                );

                $this->daoUsers->update(['password' => $password], ['id' => $user->id]);

//                if ($user->login == '18096606646') {
                $this->sendMailNotification($user, $this->convertTemlate($user->name, $user->login, $first));
                $this->sendSmsNotification($user, $first);
//                }
            }


            $offset += $amount;
        }
    }

    /**
     *
     * @return \Zend\ServiceManager\ServiceLocatorInterface
     */
    protected function getServiceManager()
    {
        return $this->serviceManager;
    }

    protected function setServiceManager(\Zend\ServiceManager\ServiceLocatorInterface $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function convertTemlate($userName, $phone, $password)
    {
        return str_replace(["{{NAME}}", "{{PHONE}}", "{{PASSWORD}}"], [$userName, $phone, $password], $this->template);
    }

    public function readTemplate($path)
    {
        $this->template = file_get_contents($path);
        if ($this->template === false) {
            throw new \Exception(__METHOD__ . " Can't read the template " . $path);
        }
    }

    protected function getMailSevice()
    {

        if (is_null($this->mailService)) {
            $this->mailService = new \Mail\GatewayProviders\AmazonSes(array(
                'key' => $this->getGlobalConfigs()->amazon_ses->aws_access_key_id,
                'secret' => $this->getGlobalConfigs()->amazon_ses->aws_secret_access_key,
                'aws_access_key_id' => $this->getGlobalConfigs()->amazon_ses->aws_access_key_id,
                'aws_secret_access_key' => $this->getGlobalConfigs()->amazon_ses->aws_secret_access_key,
                'region' => $this->getGlobalConfigs()->amazon_ses->region,
            ));
        }

        return $this->mailService;
    }

    /**
     *
     * @param type $serviceManager
     * @return \Zend\ServiceManager\Config
     */
    protected function getGlobalConfigs()
    {
        return $this->getServiceManager()->get('globalConfig');
    }

    protected function sendMailNotification($userData, $text)
    {

        if (empty($userData->email)) {
            return false;
        }

        $mailParams = new \Mail\MailParams();
        $mailParams
            ->setFromAddress("invitations@citoopus.com")
            ->setToAddresses([$userData->email])
            ->setMessageSubject("Приглашаем Вас в ваш личный офис")
            ->setBodyHtml($text)
            ->setReplyToAddresses(['twh11mail@gmail.com'])
            ->setReturnPath('twh11mail@gmail.com');

        $this->getLogCli()->log(\Log\Logger::OK, "Sent email");
        $result = $this->getMailSevice()->send($mailParams);
        $resultArr = $result->getAll();
        $data = [
            'date' => \Helpers\DateTime::create()->now(),
            'email' => $userData->email,
            'status' => 0,
            'text' => 'приглашение нового пользователя ' . $userData->id,
            'message_id' => empty($resultArr['MessageId']) ? null : $resultArr['MessageId'],
            'request_id' => empty($resultArr['ResponseMetadata']['RequestId']) ? null : $resultArr['ResponseMetadata']['RequestId'],
        ];
        $this->daoLogEmail->insert($data);
        return true;
    }

    /**
     *
     * @return \Sms\iSms
     */
    protected function getSmsSevice()
    {
        if (is_null($this->smsService)) {
            $this->smsService = new \Sms\GatewayProviders\Plivo(
                $this->getGlobalConfigs()->sms_plivo->account_id, $this->getGlobalConfigs()->sms_plivo->auth_tokem
            );
        }
        return $this->smsService;
    }

    protected function sendSmsNotification($userData, $password)
    {
        if (empty($userData->login)) {
            return false;
        }

        $text = "Приглашаем на citoopus.com - ваш личный кабинет. Ваш пароль - " . $password;

        $this->getLogCli()->log(\Log\Logger::OK, "Send sms");


//        if ($userData->login[0] == 1) {
//            $number = '+12032901389';
//        } else {
//            $number = '+79026517464';
//        }
        $number = '12032901389';

        $result = $this->getSmsSevice()->send($number, $userData->login, $text);

        $status = $result['status'] == 202 ? \Consts\Table\LogSms\Status::DELIVERED : \Consts\Table\LogSms\Status::ERROR;

        $data = [
            'date' => \Helpers\DateTime::create()->now(),
            'phone' => $userData->login,
            'status' => $status,
            'text' => $text,
            'api_id' => empty($result['response']['api_id']) ? null : $result['response']['api_id'],
            'message_uuid' => empty($result['response']['message_uuid'][0]) ? null : $result['response']['message_uuid'][0],
        ];
        $this->daoLogSms->insert($data);
        var_dump($result);
        return true;
    }

    /**
     *
     * @return \Zend\Log\LoggerInterface
     */
    protected function getLogCli()
    {
        if ($this->logCli === null) {
            return $this->getServiceManager()->get('log_cli');
        }
        return $this->logCli;
    }
}

$sendInvitations = new \SendInvitaionsToUsersByEmail($serviceManager);
$sendInvitations->run();



