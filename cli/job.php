<?php

/**
 * Скрипт запуска jobs.
 *
 */
//Очистка и начальные переменные
system("clear");
mb_internal_encoding('UTF-8');
error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set('memory_limit', '512M');
set_time_limit(0);

//Глобальная константа коренной папки
defined('ROOT_PATH') || define('ROOT_PATH', realpath(__DIR__ . '/../'));

//Подключение автозагрузки классов
require_once ROOT_PATH . '/vendor/autoload.php';
require_once ROOT_PATH . '/cli/utils/simple_html_dom.php';


//Конфигурация лога
$processor = new \Log\Processor\Backtrace();
$formatter_cli = new \Log\Formatter\ColorCli();
$log_cli = new \Log\Logger();
$writerStream = new \Zend\Log\Writer\Stream('php://output');
$writerStream->setFormatter($formatter_cli);
$log_cli->addProcessor($processor);
$log_cli->addWriter($writerStream);


//Конфигурация базы
$dbConfigs = new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/global.php');

//Подключение локального конфига базы
$localConfig = ROOT_PATH . '/cli/config/db/local.php';
if (file_exists($localConfig)) {
    $dbConfigs->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/db/local.php'));
}

$db = new \Zend\Db\Adapter\Adapter($dbConfigs->db->toArray());


//Распарсевание параметров командной строки
$options = getopt("j:m:s:h");

if (isset($options['h'])) {
    printHelp();
}


if (!isset($options['j'])) {
    $log_cli->err("Job is not specified");
    printHelp();
    die();
}

$name = $options['j'];
$idStatic = isset($options['s']) ? $options['s'] : 0;


//Подключение пускового файла
$file = ROOT_PATH . '/cli/jobs/' . $name . '.php';

if (!file_exists($file)) {
    $log_cli->err("File does not exists " . $file);
    die();
}


//Глобальный конфиг
$jobsConfigArr = require ROOT_PATH . '/cli/config/jobs.config.php';


$grabberFullLaunchCommand = implode(" ", $argv);

if (!Process::check($grabberFullLaunchCommand)) {
    $log_cli->err('Script has been already running');
    exit(1);
}

//Конфиг сервис менеджера
$smGlobalConfig = [];
if (isset($jobsConfigArr['service_manager'])) {

    $smGlobalConfig = $jobsConfigArr['service_manager'];
    unset($jobsConfigArr['service_manager']);
}

$globalConfig = new \Zend\Config\Config($jobsConfigArr);

//Подключение локального конфига
$localJobConfig = ROOT_PATH . '/cli/config/jobs.config.local.php';
if (file_exists($localJobConfig)) {
    $globalConfig->merge(new \Zend\Config\Config(require ROOT_PATH . '/cli/config/jobs.config.local.php'));
}

//Сервис менеджер
$serviceManager = new \Zend\ServiceManager\ServiceManager();
//регистрируем как сервис уже готовые объекты
$serviceManager->setService('db', $db);
$serviceManager->setService('globalConfig', $globalConfig);
$serviceManager->setService('log_cli', $log_cli);
$serviceManagerConf = new \Zend\Mvc\Service\ServiceManagerConfig($smGlobalConfig);
$serviceManagerConf->configureServiceManager($serviceManager);


/**
 * Доступные конфиги граббера
 * @global \Log\Logger $log_cli
 * @param $pathToGrabberConfigDir
 */
function printAvailableConfig($pathToGrabberConfigDir)
{
    global $log_cli;

    $itr = new \DirectoryIterator($pathToGrabberConfigDir);

    if (iterator_count($itr) > 2) {
        $log_cli->info("Available configs:");
    } else {
        $log_cli->notice("No available configs.");
    }

    foreach ($itr as $item) {
        if ($item->isFile()) {
            if (preg_match("/[a-zA-Z0-9]+\.config\.php$/", $item->getFilename())) {
                echo "[+] " . $item->getFilename() . "\n";
            }
        }
    }
    echo "\n";
}

function printHelp()
{
    echo "\n";
    echo <<<EOF
----- Grabbers Help -----
  -j    jobs name (same name as folder in cli/grabbers)
  -- Optional params --
  -m    mode name
  -s  Static ID
  -h    help
    
Examples:    
    php job.php -j do_some_job

-------------------------
EOF;
    echo "\n";
    echo "\n";
    die();
}


/*xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
$XHPROF_ROOT = "/usr/share/php/";*/

require_once $file;

/*$output = xhprof_disable();

include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_lib.php";
include_once $XHPROF_ROOT . "/xhprof_lib/utils/xhprof_runs.php";


$xhprof_runs = new XHProfRuns_Default();

// save the run under a namespace "xhprof_foo"
$run_id = $xhprof_runs->save_run($output, "xhprof_foo");*/


